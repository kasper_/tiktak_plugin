import {getOnePoint, getSubtract} from "./functions/dateHelper";

class data_class {
    static events = {};
    static resources = {};
    static sub_resources = {};
    static start_date = new Date();
    static end_date = 0;
    static points = 0;

    static setEvents(events) {
        if (this.end_date === 0) {
            this.end_date = new Date(this.start_date.getTime() + getOnePoint());
        }
        for (let key in events) {
            let event = events[key];
            this.events[event.id] = event;
            if (this.isDate(event.start)){
                let t = new Date(event.start);
                if (this.start_date.getTime() > t.getTime()) {
                    this.start_date = t;
                }
            }
            if (this.isDate(event.end)) {
                let t = new Date(event.end);
                if (this.end_date.getTime() < t.getTime()) {
                    this.end_date = t;
                }
            }
        }
        this.points = getSubtract(this.start_date,this.end_date)+1;
    }

    static setResources(resources) {
        for (let key in resources) {
            let resource = resources[key];
            this.resources[resource.id] = resource;
        }
    }
    static num_rows(resources) {
        let i = 0;
        for (let key in resources) {
            let resource = resources[key];
            this.resources[resource.id] = resource;
            i++;
            if (resource.hasOwnProperty('children')) {
                i+=resource.children.length;
                //console.log(i);
            }
        }
        return i;
    }

    static setSubResources(sub_resources) {
        for (let key in sub_resources) {
            let sub_resource = sub_resources[key];
            this.sub_resources[sub_resource.id] = sub_resource
        }
    }

    static isDate(strDate) {
        let result = false;
        if (strDate == null) {
            return result;
        }
        if (strDate === "") {
            return result;
        }
        if (strDate === "0") {
            return result;
        }
        if (strDate === 0) {
            return result;
        }
        return true;
    }
}

export default data_class;