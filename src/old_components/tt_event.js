import React from 'react'
import {getSubtract} from "../functions/dateHelper";
import TiktakComponent from "../tiktak_component";
import {config} from "../core/config";
import TResource from "./tt_resource";
import {format} from "../functions/dateHelper";

class TEvent extends React.Component{

    state = {
        mouseDown: false
    };

    static countEventsInBlockProject = {};
    static countEventsInBlockProject2 = {};

    constructor(){
        super();
        this.onClick = this.onClick.bind(this);
        this.onMouseDown = this.onMouseDown.bind(this);
        this.onMouseMove = this.onMouseMove.bind(this);
        this.onMouseUp = this.onMouseUp.bind(this);

        // if item from event object is not set in "this.itemTypes" - it don't show in modal form
        this.itemTypes = {
            name: {
                type: "textarea",
                label: "Text"
            },
            start: {
                type: "date",
                label: "Date add"
            },
            end: {
                type: "date",
                label: "Date end"
            },
            responsible_uid: {
                type: "select",
                label: "Select responsible user"
            },
            sub_tasks: {
                type: "table",
                label: "View all sub tasks"
            }
        };
    }

    onClick = function () {
        let context = this.formContext();
        let values = {};
        for (let key in this.props.event)
        {
            let active = (this.itemTypes[key] !== undefined);
            values[key] = {
                onChange: this.props.activeEvents.onChangeElement,
                onCustom: this.props.activeEvents.onCustomElement,
                type: (active && this.itemTypes[key].type),
                active: active,
                label: (active && this.itemTypes[key].label)
            };
        }
        let data = {
            data: this.props.event,
            values: values
        };
        this.props.activeEvents.showDetails(context,data);
    };

    formContext = function() {
        //console.log(this.props.event);
        return <div >{this.props.event.name}</div>
    };

    onMouseDown = (e) => {
        this.props.activeEvents.onMouseDown(e,this.refs["e_" + this.event.id],this);
    };

    onMouseMove = (e) => {
        this.props.activeEvents.onMouseMove(e,this.refs["e_" + this.event.id],this);
    };

    onMouseUp = (e) => {
        this.props.activeEvents.onMouseUp(e,this.refs["e_" + this.event.id],this);
    };
    onMouseOver = (e) => {
        this.props.activeEvents.onMouseOver(e,this.refs["e_" + this.event.id],this);
    };

    static not_active = {};
    static not_assigned = {};

    static addEventToGroup = (event, resource_id, group) => {

    };

    // reDrawResource(event){
    //     //let ids = event.resource.split("_");
    //     let start = new Date(event.start);
    //     let end = new Date(event.end);
    //     if (!TiktakComponent.allResource.hasOwnProperty("u_" + event.resource)){
    //         TiktakComponent.allResource["u_" + event.resource] = {
    //             minDate: start.getTime(),
    //             maxDate: end.getTime()
    //         }
    //     } else {
    //         if (TiktakComponent.allResource["u_" + event.resource]['minDate'] > start.getTime()){
    //             TiktakComponent.allResource["u_" + event.resource]['minDate'] = start.getTime();
    //         }
    //         if (TiktakComponent.allResource["u_" + event.resource]['maxDate'] < end.getTime()){
    //             TiktakComponent.allResource["u_" + event.resource]['maxDate'] = end.getTime();
    //         }
    //     }
    // };

    // static drawNonAssigned(parent,matrix,showDetail,activeEvents = {}){
    //     let html = [];
    //     let iteration = 0;
    //     if (TEvent.not_assigned.hasOwnProperty(parent.id)){
    //         for (let key in TEvent.not_assigned[parent.id]){
    //             let obj = TEvent.not_assigned[parent.id][key];
    //
    //             html.push(<TEvent
    //                 key={obj.id}
    //                 showDetails={showDetail}
    //                 left_points={iteration}
    //                 matrix={matrix}
    //                 onChangeElement={activeEvents.onChangeElement}
    //                 onCustomElement={activeEvents.onCustomElement}
    //                 onMouseDown={activeEvents.onMouseDown}
    //                 onMouseUp={activeEvents.onMouseUp}
    //                 onMouseMove={activeEvents.onMouseMove}
    //                 onMouseOver={activeEvents.onMouseOver}
    //             >
    //                 {obj}
    //                 </TEvent>);
    //             iteration++;
    //         }
    //     }
    //     return html;
    // }

    // static addToNonAssigned(event){
    //     //let ids = event.resource.split("_");
    //     if (isNaN(event.responsible_uid) || (event.responsible_uid === 0)){
    //         if (!TEvent.not_assigned.hasOwnProperty(event.resource)){
    //             TEvent.not_assigned[event.resource] = {};
    //         }
    //         TEvent.not_assigned[event.resource][event.id] = event;
    //     }
    //     return Object.keys(TEvent.not_assigned[event.parent_id]).length;
    // }

    static transformEventDataFromElement(element,event,tableType = 1){
        if (tableType === 1){
            let t = parseInt(element.style.width);
            let width = Math.floor(t/config.width_cell);
            t =  parseInt(element.style.left);
            let marginL = Math.floor(t/config.width_cell);
            t =  parseInt(element.style.top);
            let marginT = Math.floor(t/(config.height_cell*2));
            event.start = format(TiktakComponent.datesArray[marginL].time);
            event.end = format(TiktakComponent.datesArray[marginL+width].time);
            event.duration = getSubtract(TiktakComponent.datesArray[marginL].time,TiktakComponent.datesArray[marginL+width].time);
            if (TiktakComponent.pointRes[marginT].type === 1) {
                event.responsible_uid = 0;
                event.resource = TiktakComponent.pointRes[marginT].element.id;
            } else {
                event.resource = TiktakComponent.pointRes[marginT].parent.id;
                event.responsible_uid = TiktakComponent.pointRes[marginT].element.id;
            }
        }
        // id: 7, prototype event
        //     name: "event 7",
        //     start: "2018-08-11",
        //     end: "2018-09-21",
        //     resource: "6",
        //     color: "#fdf093",
        //     responsible_uid: 2
    }

    static transformEventAfterDrag = (event, element, field, matrix, typeTable = 1) => {
        if (typeTable === 1){
            let style;
            switch (field.type)
            {
                case 1:
                    event.resource = field.item_id;
                    element.style.width = (config.width_cell) + "px";
                    element.style.height = (config.height_cell*2+1) + "px";
                    element.style.backgroundColor = config.no_active_color;
                    style = TResource.fixEventInBody(event,matrix);
                    for (let key in style){
                        element.style[key] = style[key];
                    }
                    break;
                case 2:
                    event.resource = field.item_id;
                    element.style.height = (config.height_cell) + "px";
                    element.style.backgroundColor = event.event.color;
                    break;
                case 3:
                    event.resource = field.item_id;
                    element.style.width = (config.width_cell) + "px";
                    element.style.height = (config.height_cell) + "px";
                    element.style.backgroundColor = config.no_active_color;
                    style = TResource.fixEventOnDown(event,matrix);
                    for (let key in style){
                        element.style[key] = style[key];
                    }
                    break;
                default:

                    break;
            }
        }
    };

    // countNonAssigned(parent_id){
    //     if (TEvent.not_assigned.hasOwnProperty(parent_id)){
    //         return Object.keys(TEvent.not_assigned[parent_id]).length;
    //     }
    //     return 0;
    // }

    // addToNonActive(event){
    //     if (!TEvent.not_active.hasOwnProperty(event.resource)){
    //         TEvent.not_active[event.resource] = {};
    //     }
    //     TEvent.not_active[event.resource][event.id] = event;
    //     return Object.keys(TEvent.not_active[event.resource]).length;
    // }
    style = {
        position: "absolute",
        zIndex: 2,
        height: "" + config.height_cell + "px",
        textAlign: "center",
        cursor: "pointer",
        overflow: "hidden",
        userSelect: "none"
    };
    componentWillMount(){

        // this.event = this.props.event;
        // this.resource = TiktakComponent.resources[this.event.resource];
        // let active = (this.event.duration !== 0);
        // let assigned = (this.event.children_id !== undefined && this.event.children_id !== 0);
        // if (assigned){
        //     if (active){
        //         this.reDrawResource(this.event);
        //         this.style['width'] = "" + (config.width_cell*this.event.duration+this.event.duration-1) + "px";
        //         this.style['left'] = "" + (this.props.matrix[getSubtract(TiktakComponent.minDate,new Date(this.event.start))][this.event.full_parent]["1"]['left']) + "px";
        //         this.style['top'] = "" + (this.props.matrix[getSubtract(TiktakComponent.minDate,new Date(this.event.start))][this.event.full_parent]["1"]['top']) + "px";
        //         this.style['backgroundColor'] = this.event.color;
        //     } else {
        //         if (TiktakComponent.allResource.hasOwnProperty("r_" + this.event.resource)){
        //             let count = this.addToNonActive(this.event);
        //             let marginL = 0;
        //             if (TEvent.countEventsInBlockProject2['p_' + this.event.resource.id] !== undefined){
        //                 marginL = TEvent.countEventsInBlockProject2['p_' + this.event.resource.id]
        //             }
        //             let leftPos = getSubtract(TiktakComponent.minDate,new Date(TiktakComponent.allResource["r_" + this.event.resource]['minDate']));
        //             this.style['width'] = "" + config.width_cell + "px";
        //             this.style['top'] = "" + (this.props.matrix["rn_" + this.event.full_parent]['top']) + "px";
        //             this.style['backgroundColor'] = config.no_active_color;
        //             this.style['left'] = "" + (config.width_cell*(marginL-1)+marginL-1+leftPos*config.width_cell+leftPos) + "px";
        //             TEvent.countEventsInBlockProject2['p_' + this.event.resource.id] = marginL+1;
        //         }
        //     }
        // } else {
        //     let leftPos = getSubtract(TiktakComponent.minDate,new Date(TiktakComponent.allResource["r_" + this.resource.id]['minDate']));
        //     //let leftPosMax = getSubtract(TiktakComponent.minDate,new Date(TiktakComponent.allResource["r_" + obj.id]['maxDate']));
        //     let marginL = 0;
        //     if (TEvent.countEventsInBlockProject['p_' + this.resource.id] !== undefined){
        //         marginL = TEvent.countEventsInBlockProject['p_' + this.resource.id]
        //     }
        //     this.style['backgroundColor'] = config.no_active_color;
        //     this.style['width'] = config.width_cell + "px";
        //     this.style['height'] = (config.height_cell*2+1) + "px";
        //     this.style['top'] = (this.props.matrix["r_" + this.event.resource]["top"]) + "px";
        //     this.style['left'] = (leftPos*config.width_cell + leftPos + marginL*config.width_cell + marginL) + "px";
        //     //this.countEventsInBlockProject['p_' + this.resource.id] = ;
        //     TEvent.countEventsInBlockProject['p_' + this.resource.id] = marginL+1;
        //
        //     //this.style['left'] = "" + (config.width_cell*(this.props.left_points) + (this.props.left_points)) + "px";
        // }
        // let style = this.props.style;
        //
        // for (let key in style){
        //     this.style[key] = style[key];
        // }
        //
    }
    render(){

        let style = {};
        for (let key in this.style) {
            style[key] = this.style[key];
        }
        for (let key in this.props.style) {
            style[key] = this.props.style[key];
        }
        this.event = this.props.event;
        let html = [];
        if (TiktakComponent.events[this.event.id].active) {
            html.push(<p key="left_resize" style={{
                height: config.height_cell,
                margin: 0,
                width: "6px",
                left: "4px",
                position: "absolute",
                cursor: "col-resize",
                zIndex: 3,
                top:0
            }}/>);
            html.push(<p key="right_resize" style={{
                height: config.height_cell,
                    margin: 0,
                    width: "6px",
                    right: "4px",
                    position: "absolute",
                    cursor: "col-resize",
                    zIndex: 3,
                    top:0
            }}/>);
        }
        return <div ref={"e_" + this.event.id} onMouseOver={(e) => this.onMouseOver(e)} onClick={this.onClick} onMouseDown={(e) => this.onMouseDown(e)} onMouseMove={(e) => this.onMouseMove(e)} onMouseUp={(e) => this.onMouseUp(e)} event_id={this.event.id} style={style}>{this.event.name}{html}</div>
    }

}

export default TEvent;