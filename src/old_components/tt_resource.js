import React from 'react'
import {getSubtract} from "../functions/dateHelper"
import TiktakComponent from '../tiktak_component'
import {config} from '../core/config'
import TEvent from './tt_event'

class TResource extends React.Component{

    onClick = () => {

    };
    
    static fixEventInBody(event,matrix) {
        let resource = TiktakComponent.resources[event.event.resource];
        if (TiktakComponent.allResource.hasOwnProperty("r_" + resource.id)){
            let leftPos = getSubtract(TiktakComponent.minDate,new Date(TiktakComponent.allResource["r_" + resource.id]['minDate']));
            if (!resource.hasOwnProperty('eventsNonAssign')){
                resource['eventsNonAssign'] = {};
                resource['eventsNonAssign'][event.event.id] = 0;
            }
            let marginL = resource['eventsNonAssign'][event.event.id];

            return {
                backgroundColor: config.no_active_color,
                width: config.width_cell + "px",
                height: (config.height_cell*2+1) + "px",
                top: (matrix["r_" + event.resource]["top"]) + "px",
                left: (leftPos*config.width_cell + leftPos + marginL*config.width_cell + marginL) + "px"
            }
        }
    }
    static fixEventOnDown(event,matrix) {
        let resource = TiktakComponent.resources[event.resource];
        if (TiktakComponent.allResource.hasOwnProperty("r_" + resource.id)){
            let leftPos = getSubtract(TiktakComponent.minDate,new Date(TiktakComponent.allResource["r_" + resource.id]['minDate']));
            let marginL = 0;
            if (TEvent.countEventsInBlockProject2['p_' + resource.id] !== undefined){
                marginL = TEvent.countEventsInBlockProject2['p_' + resource.id]
            }
            TEvent.countEventsInBlockProject2['p_' + resource.id] = marginL+1;
            return {
                backgroundColor: config.no_active_color,
                width: config.width_cell + "px",
                height: (config.height_cell) + "px",
                left: (leftPos*config.width_cell + leftPos + marginL*config.width_cell + marginL) + "px"
            }
        }
    }

    render () {
        let obj = this.props.children;
        switch (this.props.type){
            case 1:

                if (!TiktakComponent.allResource.hasOwnProperty("r_" + obj.id)){
                    return <div key={"res" + obj.id}/>;
                }
                let leftPos = getSubtract(TiktakComponent.minDate,new Date(TiktakComponent.allResource["r_" + obj.id]['minDate']));
                let leftPosMax = getSubtract(TiktakComponent.minDate,new Date(TiktakComponent.allResource["r_" + obj.id]['maxDate']));
                let width = leftPosMax - leftPos;
                if (!this.props.matrix.hasOwnProperty("r_" + obj.id)){
                    console.log(obj);
                    console.log(this.props.matrix);
                }
                let style = {
                    position: "absolute",
                    zIndex: 1,
                    top: this.props.matrix["r_" + obj.id]["top"] + "px",
                    left: (leftPos*config.width_cell + leftPos) + "px",
                    width: (width*config.width_cell + (width-1)) + "px",
                    backgroundColor: obj.color,
                    height: (config.height_cell*2+1) + "px",
                    overflow: "hidden"
                };
                return <div key={"res" + obj.id} style={style}>
                    {
                    }
                    </div>;
            default:
                return <div key={"res" + obj.id}/>
        }
    }
}

export default TResource;