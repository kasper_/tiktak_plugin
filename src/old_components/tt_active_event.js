import React from 'react'
import TEvent from "./tt_event";
import {getSubtract} from '.././functions/dateHelper'
import {config} from ".././core/config"
import TiktakComponent from ".././tiktak_component"

class TActiveEvent extends React.Component {
    state = {

    };

    render () {
        let htmlEvents = [];
        for (let key in this.props.events){
            let event = this.props.events[key];
            console.log(getSubtract(TiktakComponent.minDate,new Date(event.start)));
            // if (this.props.matrix[getSubtract(TiktakComponent.minDate,new Date(event.start))][event.full_parent] === undefined);
            // {
            //     console.log(event);
            // }
            let style = {
                width: (config.width_cell*event.duration+event.duration-1) + "px",
                left: (this.props.matrix[getSubtract(TiktakComponent.minDate,new Date(event.start))][event.full_parent]["1"]['left']) + "px",
                top: (this.props.matrix[getSubtract(TiktakComponent.minDate,new Date(event.start))][event.full_parent]["1"]['top']) + "px",
                backgroundColor: event.color
            };
            htmlEvents.push(<TEvent key={event.id} event={event} activeEvents={this.props.activeEvents} style={style}/>)
        }
        //console.log(htmlEvents);
        return htmlEvents;
    }
}

export default TActiveEvent;