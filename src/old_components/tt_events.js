import React from 'react'
import TEvent from "./tt_event";
import {config} from "../core/config";
import TModalWindow from "./tt_modal_window"
import TNotActiveEvent from "./tt_not_active_event";
import TNotAssignEvent from "./tt_not_assign_event";
import TActiveEvent from "./tt_active_event";
import TiktakComponent from "../tiktak_component";
import {resource} from "../db/db";

class TEvents extends React.Component {

    state = {
        isModalVisible: false,
        modalContent: "",
        modalData: {},
        events: this.props.events
    };

    mouseDown = false;
    mouseDrag = false;
    mouseResize = false;

    static html;

    showDetail = (item, data = {}) => {
        if (!this.mouseDrag){
            this.setState({
                isModalVisible: true,
                modalContent: item,
                modalData: data,
            });
        } else {
            this.mouseDrag = false;
        }
    };

    tempEventValues = {};

    onChangeElement = (name, value) => {
        this.tempEventValues[name] = value;
    };

    onMouseDown = (e,element,data) => {
        this.mouseDown = true;
        this.startX = e.pageX;
        this.startY = e.pageY;
        this.startLeft = parseInt(element.style.left);
        this.startTop = parseInt(element.style.top);
    };

    onStartResize = (e,element,data) => {

    };

    onEndResize = (e,element, data) => {

    };

    onMouseUp = (e,element,data) => {
        this.mouseDown = false;
        if (this.mouseDrag) {
            this.stopDrag(e,element,data);
        }
    };
    onMouseOver = (e,element,data) => {
        this.mouseDown = false;
        if (this.mouseDrag) {
            this.stopDrag(e,element,data);
            this.mouseDrag = false;
        }
    };
    startX = 0;
    startY = 0;
    startLeft = 0;
    startTop = 0;
    onMouseMove = (e,element,data) => {
        if (this.mouseDown) {
            // console.log(e.target.tagName);
            if (e.target.tagName === "P"){
                if (!this.mouseResize){
                    this.mouseResize = true;
                    this.onStartResize(e,element,data);
                }
                if (this.mouseResize){
                    let left = this.startLeft;
                    let x = e.pageX - this.startX;
                    element.style.left = (x + left) + 'px';
                    //this.onReElement(e,element,data);
                }
            } else {
                if (!this.mouseDrag){
                    this.mouseDrag = true;
                    this.startDrag(e,element,data);
                }
                if (this.mouseDrag){
                    let left = this.startLeft;
                    let top = this.startTop;
                    let x = e.pageX - this.startX;
                    let y = e.pageY - this.startY;
                    element.style.left = (x + left) + 'px';
                    element.style.top = (y + top) + 'px';
                    this.onDragElement(e,element,data);
                }
            }

        } else {
            this.mouseDrag = false;
        }


    };

    startDrag(e,element,data){
        element.style.zIndex = element.style.zIndex * 1 + 2;
        element.style.cursor = "move";
        this.onStartDragElement(e,element,data);
    }
    stopDrag(e,element,data){
        if (element.style.zIndex > 4){
            element.style.zIndex -= 2;
            element.style.cursor = "pointer";
        }
        let marginL = Math.round(parseInt(element.style.left)/(config.width_cell+1));
        let marginT = Math.round(parseInt(element.style.top)/(config.height_cell+1));
        element.style.left = (marginL*config.width_cell + marginL) + "px";
        element.style.top = (marginT*config.height_cell + marginT+1) + "px";

        if (this.props.matrix['types_row'].hasOwnProperty(marginT)){
            let field = this.props.matrix['types_row'][marginT];
            TEvent.transformEventAfterDrag(data,element,field,this.props.matrix);
        }
        //this.props.setState();
        this.onEndDragElement(e,element,data);
        this.startX = 0;
        this.startY = 0;
        this.startLeft = 0;
        this.startTop = 0;
        TEvent.transformEventDataFromElement(element,data.event);
        TiktakComponent.events[data.event.id] = data.event;
        //let events = TiktakComponent.events;
        // this.setState({events: events});
        console.log(TiktakComponent.events);
    }

    onStartDragElement = (e,element,data) => {

    };

    onDragElement = (e,element,data) => {

    };

    onEndDragElement = (e,element,data) => {

    };

    onCustomElement = (data, field = {}) => {

    };

    onAfterSave = (event) => {

    };

    closeModal = () => this.setState({isModalVisible: false});
    doAction = data => {
        let events = this.props.events;
        for (let key in this.tempEventValues){
            data[key] = this.tempEventValues[key];
        }
        data['full_parent'] = data.resource + "_" + data.responsible_uid;
        events[data.id] = data;
        this.setState({events: events});
        this.onAfterSave(data);
        this.tempEventValues = {};
    };

    render () {
        let events = this.state.events;
        //console.log(events);
        let sortEvents = {t1: {},t2: {},t3: {},t4: {}};
        for (let key in events){
            //console.log(TiktakComponent.resources);
            let event = events[key];
            if (event.responsible_uid !== undefined && event.responsible_uid !== 0 && event.responsible_uid !== "0"){
                if (event.duration !== 0){
                    sortEvents.t1[event.id] = event;
                    TiktakComponent.events[event.id]['active'] = true;
                    if (!TiktakComponent.resources[event.resource].hasOwnProperty("eventsActive")) {
                        TiktakComponent.resources[event.resource]["eventsActive"] = {};
                    }
                    TiktakComponent.resources[event.resource]["eventsActive"][event.id] = Object.keys(TiktakComponent.resources[event.resource]["eventsActive"]).length;
                } else {
                    sortEvents.t2[event.id] = event;
                    TiktakComponent.events[event.id]['active'] = false;
                    if (!TiktakComponent.resources.hasOwnProperty(event.resource)){
                        console.log(event);
                    }
                    if (!TiktakComponent.resources[event.resource].hasOwnProperty("eventsNonActive")) {
                        TiktakComponent.resources[event.resource]["eventsNonActive"] = {};
                    }
                    TiktakComponent.resources[event.resource]["eventsNonActive"][event.id] = Object.keys(TiktakComponent.resources[event.resource]["eventsNonActive"]).length;
                }
            } else {
                if (event.duration !== 0){
                    sortEvents.t3[event.id] = event;
                    TiktakComponent.events[event.id]['active'] = false;
                    if (!TiktakComponent.resources[event.resource].hasOwnProperty("eventsNonAssign")) {
                        TiktakComponent.resources[event.resource]["eventsNonAssign"] = {};
                    }
                    TiktakComponent.resources[event.resource]["eventsNonAssign"][event.id] = Object.keys(TiktakComponent.resources[event.resource]["eventsNonAssign"]).length;
                } else {
                    sortEvents.t4[event.id] = event;
                    TiktakComponent.events[event.id]['active'] = false;
                }
            }
        }
        console.log(TiktakComponent.resources);
        let activeEvents = {
            onChangeElement: this.onChangeElement,
            onCustomElement: this.onCustomElement,
            onMouseDown: this.onMouseDown,
            onMouseUp: this.onMouseUp,
            onMouseMove: this.onMouseMove,
            onMouseOver: this.onMouseOver,
            showDetails: this.showDetail
        };
        return <div style={this.props.style}>

            <TNotActiveEvent allEvents={sortEvents} activeEvents={activeEvents} matrix={this.props.matrix}/>
            <TNotAssignEvent allEvents={sortEvents} activeEvents={activeEvents} matrix={this.props.matrix}/>
            <TActiveEvent events={sortEvents.t1} activeEvents={activeEvents} matrix={this.props.matrix}/>
            {this.state.isModalVisible && (
            <TModalWindow close={this.closeModal} data={this.state.modalData.data} action={this.doAction} values={this.state.modalData.values}>
                {this.state.modalContent}
            </TModalWindow>
        )}</div>;
    }
}

export default TEvents;