import React from 'react'
import TEvent from './tt_event'
import {config} from "../core/config";
import TiktakComponent from "../tiktak_component"
import {getSubtract} from "../functions/dateHelper"

class TNotAssignEvent extends React.Component {
    state = {

    };

    render () {
        let htmlNotAssign = [];
        //let htmlMisk = [];
        let style;
        for (let key in this.props.allEvents.t3){ // t1 send in tt_active_event.js (t - type)
            let e = this.props.allEvents.t3[key];
            let leftPos = 0;
            if (TiktakComponent.allResource.hasOwnProperty("r_" + e.resource)){
                leftPos = getSubtract(TiktakComponent.minDate,new Date(TiktakComponent.allResource["r_" + e.resource]['minDate']));
            }
            //console.log(this.props.matrix);
            console.log(e);
            style = {
                width: config.width_cell + "px",
                top: (this.props.matrix["r_" + e.resource]['top']) + "px",
                backgroundColor: config.no_active_color,
                left: (leftPos * config.width_cell + leftPos) + "px",
                height: (config.height_cell*2+1) + "px"
            };
            htmlNotAssign.push(<TEvent key={e.id} event={e} activeEvents={this.props.activeEvents} style={style}/>);
        }
        for (let key in this.props.allEvents.t4){ // t1 send in tt_active_event.js (t - type)
            let e = this.props.allEvents.t4[key];
            let leftPos = 0;
            if (TiktakComponent.allResource.hasOwnProperty("r_" + e.resource)){
                leftPos = getSubtract(TiktakComponent.minDate,new Date(TiktakComponent.allResource["r_" + e.resource]['minDate']));
            }
            style = {
                width: config.width_cell + "px",
                top: (this.props.matrix["r_" + e.resource]['top']) + "px",
                backgroundColor: config.no_active_color,
                left: (leftPos * config.width_cell + leftPos) + "px",
                height: (config.height_cell*2+1) + "px"
            };
            htmlNotAssign.push(<TEvent key={e.id} event={e} activeEvents={this.props.activeEvents} style={style}/>);
        }

        return htmlNotAssign;
    }
}

export default TNotAssignEvent;