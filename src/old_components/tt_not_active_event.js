import React from 'react'
import TiktakComponent from '.././tiktak_component'
import {config} from '.././core/config'
import {getSubtract} from "../functions/dateHelper";
import TEvent from "./tt_event";

class TNotActiveEvent extends React.Component {
    state = {

    };

    render () {
        let htmlNotActive = [];
        //let htmlMisk = [];
        let style;
        let n = {};
        for (let key in this.props.allEvents.t2){ // t1 send in tt_active_event.js (t - type)
            let e = this.props.allEvents.t2[key];
            //console.log(key);
            if (!n.hasOwnProperty("r_" + e.resource)){
                n["r_" + e.resource] = 0;
            } else {
                n["r_" + e.resource] += 1;
            }
            if (TiktakComponent.allResource.hasOwnProperty("r_" + e.resource)){
                let leftPos = getSubtract(TiktakComponent.minDate,new Date(TiktakComponent.allResource["r_" + e.resource]['minDate']));
                //console.log(((n+leftPos) * config.width_cell) , (leftPos*1 + n));
                // if (!this.props.matrix.hasOwnProperty("rn_" + e.full_parent)){
                //     console.log("rn_" + e.full_parent);
                // }
                style = {
                    width: config.width_cell + "px",
                    top: (this.props.matrix["rn_" + e.full_parent]['top']) + "px",
                    backgroundColor: config.no_active_color,
                    left: ((n["r_" + e.resource]+leftPos) * config.width_cell + leftPos + n["r_" + e.resource]) + "px"
                }
            }
            htmlNotActive.push(<TEvent key={e.id} style={style} event={e}  activeEvents={this.props.activeEvents} />);
        }

        return htmlNotActive;
    }
}

export default TNotActiveEvent;