export const config = {
    width_cell: 35,
    height_cell: 25,
    no_active_color: "#ccc",
    weekends_color: "#eea579",
    event_color: "#fdf093",
    width_left_panel: 275
};