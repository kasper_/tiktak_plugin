import React from 'react'
import Header from "./components/header/header";
import {config} from "./core/config";
import data_class from "./data_class";
import BodyPanel from './components/body/body_panel';
import {format, getSubtract} from "./functions/dateHelper";
import HeaderLeft from "./components/header/header_left";
import LeftPanel from "./components/left_panel/left_panel";
import EventsBody from "./components/body/events_body";

class Main extends React.Component {

    constructor(props) {
        super(props);

        this.dragEvents = {};
        this.resizeEvents = {};

        this.state = {
            events: data_class.events,
            resources: data_class.resources,
            sub_resources: data_class.sub_resources
        };
        this.eventsStates = {};
    }

    init() {
        for (let key in this.props) {
            config[key] = this.props[key];
        }
        this.matrix = {
            header: {},
            resource: {},
            body: {}
        };

        // console.log(window.tiktak);
    }

    isGeneralEvent = (event) => {
        let resource = data_class.resources[event.resource];
        if (!resource.hasOwnProperty('general')) {
            return false;
        }
        return (resource.general === '1' || resource.general === 1);
    };

    reloadBackground = (event) => {
        let events = this.state.events;
        let res = this.state.resources[event.resource];
        let resources = this.state.resources;
        let fined = false;
        if (!res.hasOwnProperty("children")) {
            res['children'] = [];
        }
        if (event.responsible_uid > 0) {
            for (let key in res.children) {
                // res
                if (res.children[key].id === event.responsible_uid || "" + res.children[key].id === "" + event.responsible_uid) {
                    fined = true;
                    break;
                }
            }
            if (!fined) {
                let children = [...res.children];
                children.push({
                    id: event.responsible_uid
                });
                res.children = children;
                resources[res.id] = res;
            }
        }
        events[event.id] = event;
        if (!fined) {

            this.setState({
                resources: resources,
                events: events
            });

        }
        return fined;
    };

    doSomeWorkIfDontReload = (event) => {

    };

    componentWillUpdate() {
        this.init();
    }

    componentWillMount() {
        this.init();
    }

    componentDidMount() {
        //console.log(this.eventsStates);
        //console.log(data_class.resources,data_class.sub_resources,data_class.events);
    }

    onScroll = (e) => {
        this.scrollX = this.refs.main.scrollLeft;
        this.scrollY = this.refs.main.scrollTop;
    };

    onMouseMove = (e) => {
        let id = 0;
        let nowMouseDrag = (Object.keys(this.dragEvents).length !== 0);
        let nowMouseResize = (Object.keys(this.resizeEvents).length !== 0);
        if (!nowMouseDrag && !nowMouseResize) {
            if (!e.target.hasAttribute("event_id")) {
                return 0;
            }
            id = e.target.getAttribute('event_id')
        } else {
            // if make option multiple select - you need check this code
            if (nowMouseDrag) {
                for (let key in this.dragEvents) {
                    id = this.dragEvents[key].id;
                }
            }
            if (nowMouseResize) {
                for (let key in this.resizeEvents) {
                    id = this.resizeEvents[key].id;
                }
            }

        }
        let eventStates = this.eventsStates[id];
        //console.log(eventStates.s);
        if (eventStates.s.dobleClick) {
            eventStates.f({
                dobleClick: false,
                down: false,
                zIndex: 0
            });
        }
        let event = eventStates.s.event;
        if (eventStates.s.down) {
            this.dragEvents[event.id] = event;
            if (!eventStates.s.drag) {
                eventStates.f({
                    drag: true,
                    zIndex: 1
                });
                this.onStartDrag(e, eventStates);
            }
            this.onDrag(e, eventStates);
        } else if (eventStates.s.resizeRightDown) {
            this.resizeEvents[event.id] = event;
            if (!eventStates.s.resizeRight) {
                eventStates.f({
                    resizeRight: true,
                    zIndex: 1
                });
                this.onStartResize(e, eventStates);
            }
            this.onResizeRight(e, eventStates);
        } else if (eventStates.s.resizeLeftDown) {
            this.resizeEvents[event.id] = event;
            if (!eventStates.s.resizeLeft) {
                eventStates.f({
                    resizeLeft: true,
                    zIndex: 1
                });
                this.onStartResize(e, eventStates);
            }
            this.onResizeLeft(e, eventStates);
        }
        // console.log(eventStates.s);
        // console.log('start resize');

    };

    onStartDrag = (e, states) => {
        //console.log('start drag');
        this.scrollX = this.refs.main.scrollLeft;
        this.scrollY = this.refs.main.scrollTop;
        this.startScrollTop = this.refs.main.scrollTop;
        this.startScrollLeft = this.refs.main.scrollLeft;
        let event = states.s.event;
        let rect = e.target.getBoundingClientRect();
        this.x = e.clientX - rect.left;
        this.startX = e.pageX;
        this.startY = e.pageY;
        this.startTop = this.eventsStates[event.id].s.top;
        this.startLeft = this.eventsStates[event.id].s.left;

        if (this.eventsStates[event.id].s.type === 2) {
            this.importFunctions['operationWithBlock']("child", event, "remove");
            this.importFunctions['operationWithBlock']("child", event, "refresh");
        } else if (this.eventsStates[event.id].s.type === 3) {
            this.importFunctions['operationWithBlock']("main", event, "remove");
            this.importFunctions['operationWithBlock']("main", event, "refresh");
        }
        this.createShadow(event, this.eventsStates[event.id].s.type);
        if (this.props.globalVariables.hasOwnProperty('onStartDrag')) {
            this.props.globalVariables['onStartDrag'](event);
        }

    };

    createShadow = (event, type) => {
        let top = 0;
        let heightPoint = 0;
        let height = 0;
        if (this.isGeneralEvent(event)) {
            heightPoint = config.height_cell + 2 + config.height_cell;
            for (let key in this.matrix.resource) {
                height += heightPoint;
            }
        } else {
            let res = null;
            switch (type) {

                case 3:
                    heightPoint = config.height_cell + 2 + config.height_cell;
                    height = heightPoint;
                    for (let key in this.matrix.resource) {

                        let resource = this.matrix.resource[key];
                        if (resource.type === 'main' && resource.element.id === event.resource) {
                            res = resource;
                        }
                        if (res === null) {
                            continue;
                        }
                        if (resource.type === 'child' && resource.main.id === event.resource) {
                            height += heightPoint;
                        }
                    }
                    break;

                default:
                    heightPoint = config.height_cell + 2 + config.height_cell;
                    height = heightPoint;
                    for (let key in this.matrix.resource) {
                        let resource = this.matrix.resource[key];
                        if (resource.type === 'main' && resource.element.id === event.resource) {
                            res = resource;
                        }
                        if (res === null) {
                            continue;
                        }
                        if (resource.type === 'child' && resource.main.id === event.resource) {
                            height += heightPoint;
                        }
                    }
                    break;
            }
            top = res.top;
        }

        this.shadowBlockStates.f({
            hidden: false,
            topVisible: top,
            heightVisible: height
        });

    };

    onDrag = (e, states) => {
        let dragOn = true;
        if (this.props.globalVariables.hasOwnProperty('onBeforeDrag')) {
            dragOn = this.props.globalVariables['onBeforeDrag'](states.s.event);
            if (dragOn !== false) {
                dragOn = true;
            }
        }
        if (dragOn) {
            let top = this.startTop;
            let left = this.startLeft;
            let y = e.pageY - this.startY + this.scrollY - this.startScrollTop;
            let x = e.pageX - this.startX + this.scrollX - this.startScrollLeft;
            let d = {
                top: (y + top),
                left: (x + left),
                noDrop: false
            };
            let visible = {
                top: this.shadowBlockStates.s.topVisible,
                bottom: this.shadowBlockStates.s.heightVisible + this.shadowBlockStates.s.topVisible - 25
            };
            if (visible.top > d.top
                || visible.bottom < d.top) {
                d.noDrop = true;
            }
            states.f(d);
            if (this.props.globalVariables.hasOwnProperty('onDrag')) {
                this.props.globalVariables['onDrag'](states.s.event);
            }
        }
    };

    onEndDrag = (e, states) => {

        // console.log('end drag');
        let event = states.s.event;
        if (states.s.noDrop) {
            if (states.s.type === 3) {
                this.importFunctions['operationWithBlock']("main", event, "add");
                this.importFunctions['operationWithBlock']("main", event, "refresh");
            } else if (states.s.type === 2) {
                this.importFunctions['operationWithBlock']("child", event, "add");
                this.importFunctions['operationWithBlock']("child", event, "refresh");
            }
            let state = {
                left: this.startLeft,
                top: this.startTop,
                noDrop: false
            };
            states.f(state);
        } else {
            let top = 0;
            let left = 0;

            // let oldEvent = JSON.parse(JSON.stringify(states.s.event));
            let oldType = states.s.type;

            let halfHeightBlock = Math.floor(config.height_cell / 2);
            let halfWidthBlock = 0;

            if (oldType === 3) {
                halfHeightBlock = config.height_cell;
                // let widthInPoints = getSubtract(new Date(oldEvent.start), new Date(oldEvent.end));
                // console.log(widthInPoints);
            } else if (oldType === 1) {
                halfWidthBlock = Math.floor(config.width_cell / 2);
            }

            if (states.s.edit) {
                top = states.s.top + halfHeightBlock;
                left = states.s.left + halfWidthBlock;
            } else {
                top = this.startTop;
                left = this.startLeft;
            }
            let type = 0;
            let item = this.matrix.resource[0];
            let state = {};

            for (let key in this.matrix.resource) {
                let item_resource = this.matrix.resource[key];
                if (top < item_resource.top) {
                    break;
                } else {
                    item = this.matrix.resource[key];
                }
            }
            if (item.type === 'main') {
                type = 3;
                top = item.top;
                event.resource = item.element.id;
                event.responsible_uid = "0";
                event.end = "0";
                this.importFunctions['operationWithBlock']("main", event, "add");
                this.importFunctions['operationWithBlock']("main", event, "refresh");
            } else {
                if (top < (item.top + config.height_cell)) {
                    type = 1;
                    top = item.top;
                    let head_item = this.matrix.header[0];
                    for (let key in this.matrix.header) {
                        if (left < this.matrix.header[key].values.left) {
                            break;
                        } else {
                            head_item = this.matrix.header[key];
                        }
                    }
                    let startDate = new Date(event.start);
                    // console.log(event.start);
                    // console.log(event.end);
                    // console.log(head_item.values.date);
                    event.resource = item.main.id;
                    event.responsible_uid = item.element.id;
                    event.start = format(head_item.values.date, 2);
                    let endDate = null;
                    let widthEvent = 1;
                    if (data_class.isDate(event.end)) {
                        endDate = new Date(event.end);
                        widthEvent = getSubtract(startDate, endDate);
                        // let width = getSubtract(new Date(this.state.event.start), new Date(this.state.event.end)) + 1;
                    }
                    let width = config.width_cell * (widthEvent+1) + widthEvent;
                    state['width'] = width < config.width_cell ? config.width_cell : width;

                    let d = new Date(+head_item.values.date);
                    switch (this.props.timeline_point) {
                        case 'minute':
                            d.setMinutes(head_item.values.date.getMinutes() + widthEvent);
                            break;
                        case 'hour':
                            d.setHours(head_item.values.date.getHours() + widthEvent);
                            break;
                        default:
                            d.setDate(head_item.values.date.getDate() + widthEvent);
                            break;
                    }
                    event.end = format(d, 2);
                    state['left'] = head_item.values.left;
                } else {
                    top = item.top + config.height_cell + 1;
                    type = 2;
                    event.responsible_uid = item.element.id;
                    event.end = "0";
                    event.resource = item.main.id;
                    this.importFunctions['operationWithBlock']("child", event, "add");
                    this.importFunctions['operationWithBlock']("child", event, "refresh");
                }
            }
            state['top'] = top;
            state['type'] = type;
            states.f(state);
        }

        this.shadowBlockStates.f({
            hidden: true,
        });
        if (this.props.globalVariables.hasOwnProperty('onEndDrag')) {
            this.props.globalVariables['onEndDrag'](event);
        }

    };

    onStartResize = (e, states) => {
        // console.log(states);
        this.scrollX = this.refs.main.scrollLeft;
        this.startScrollLeft = this.refs.main.scrollLeft;
        let event = states.s.event;
        let rect = e.target.getBoundingClientRect();
        this.x = e.clientX - rect.left;
        this.startX = e.pageX;
        this.startLeft = this.eventsStates[event.id].s.left;
    };

    onEndResize = (e, states) => {
        let event = states.s.event;
        let halfWidthBlock = Math.floor(config.width_cell / 2);
        let left = states.s.left;
        if (this.endResizeRight !== 0) {
            let width = states.s.width + this.endResizeRight - halfWidthBlock;
            let head_item = this.matrix.header[0];
            for (let key in this.matrix.header) {
                if ((left+width) < this.matrix.header[key].values.left) {
                    break;
                } else {
                    head_item = this.matrix.header[key];
                }
            }
            event.end = format(head_item.values.date, 2);

            let widthEvent = getSubtract(new Date(event.start), head_item.values.date);

            width = config.width_cell * (widthEvent+1) + widthEvent;
            states.f({
                width: width
            });
        } else if (this.endResizeLeft !== 0) {

            left += this.endResizeLeft+halfWidthBlock;
            let head_item = this.matrix.header[0];
            for (let key in this.matrix.header) {
                if (left < this.matrix.header[key].values.left) {
                    break;
                } else {
                    head_item = this.matrix.header[key];
                }
            }
            event.start = format(head_item.values.date, 2);

            let endDate = new Date(event.end);
            let widthEvent = getSubtract(new Date(event.start), endDate);
                // let width = getSubtract(new Date(this.state.event.start), new Date(this.state.event.end)) + 1;

            let width = config.width_cell * (widthEvent+1) + widthEvent;

            states.f({
                width: width < config.width_cell ? config.width_cell : width,
                left: head_item.values.left
            });
        }
        if (this.props.globalVariables.hasOwnProperty('onEndResize')) {
            this.props.globalVariables['onEndResize'](event);
        }
    };

    onResizeLeft = (e, states) => {
        // console.log(states.s.resizeLeft);
        let x = e.pageX - this.startX + this.scrollX - this.startScrollLeft;
        let d = {
            resizeLeftNum: x
        };
        states.f(d);
    };

    onResizeRight = (e, states) => {
        let x = e.pageX - this.startX + this.scrollX - this.startScrollLeft;
        let d = {
            resizeRightNum: x
        };
        states.f(d);
    };

    onBeforeEndResize = (e, states) => {
        this.endResizeRight = states.s.resizeRightNum;
        this.endResizeLeft = states.s.resizeLeftNum;
    };

    onMouseUp = (e) => {
        if (Object.keys(this.dragEvents).length !== 0) {
            for (let key in this.dragEvents) {
                this.eventsStates[this.dragEvents[key].id].f({
                    drag: false,
                    down: false
                });
                this.onEndDrag(e, this.eventsStates[this.dragEvents[key].id]);
            }
            this.dragEvents = {};
        } else if (Object.keys(this.resizeEvents).length !== 0) {
            for (let key in this.resizeEvents) {
                this.onBeforeEndResize(e, this.eventsStates[this.resizeEvents[key].id]);
                this.eventsStates[this.resizeEvents[key].id].f({
                    resizeLeft: false,
                    resizeRight: false,
                    resizeLeftDown: false,
                    resizeRightDown: false,
                    resizeRightNum: 0,
                    resizeLeftNum: 0,
                    zIndex: 0
                });
                this.onEndResize(e, this.eventsStates[this.resizeEvents[key].id]);
            }
            this.resizeEvents = {};
        }

    };

    componentDidUpdate() {
        //console.log(this.eventsStates);
    }

    render() {
        this.importFunctions = {};
        let widthBody = data_class.points * config.width_cell + data_class.points;
        let num_rows = data_class.num_rows(data_class.resources);
        let body_height = num_rows * (config.height_cell * 2 + 1) + num_rows;

        let header = <Header data={this.matrix.header} start={data_class.start_date} end={data_class.end_date}/>;
        let headerLeft = <HeaderLeft/>;
        let leftPanel = <LeftPanel
            data={this.matrix.resource}
            resources={this.state.resources}
            sub_res={this.state.sub_resources}
            importFunctions={this.importFunctions}
            style={{
                width: (config.width_left_panel - 2) + "px",
                backgroundColor: "#fff",
                position: "absolute",
                top: (config.height_cell + 2) + "px",
                left: "1px",
                userSelect: "none"
            }}
        />;
        this.shadowBlockStates = {
            f: null,
            s: null
        };
        let body = <BodyPanel
            header={this.matrix.header}
            left={this.matrix.resource}
            body={this.matrix.body}
            shadowBlockStates={this.shadowBlockStates}
            widthBody={widthBody}
            heightBody={body_height}
        />;


        let eventsBody = <EventsBody
            width={widthBody}
            height={body_height}
            header={this.matrix.header}
            left={this.matrix.resource}
            resources={this.state.resources}
            sub_res={this.state.sub_resources}
            events={this.state.events}
            reloadBackground={this.reloadBackground}
            eventsStates={this.eventsStates}
            dragEvents={this.dragEvents}
            importFunctions={this.importFunctions}
            globalVariables={this.props.globalVariables}
        />;
        return <div
            ref="main"
            onMouseMove={(e) => this.onMouseMove(e)}
            onMouseUp={(e) => this.onMouseUp(e)}
            onScroll={(e) => this.onScroll(e)}
            style={{
                height: this.props.height + "px",
                overflowY: "scroll",
                position: 'relative',
                width: (data_class.points * config.width_cell + config.width_left_panel + data_class.points + 15) + "px", //(15 px - scroll-bar)
                backgroundColor: "#ccc"
            }}>
            {leftPanel}
            {headerLeft}
            {header}
            {body}
            {eventsBody}
        </div>
    }

}

export default Main;