import React from 'react'
import ReactDOM from 'react-dom'
import {resource as resources, events, sub_resources} from './db/db'
import Main from "./main";
import data_class from './data_class';
import {config} from "./core/config";
// import TiktakComponent from './tiktak_component'
let globalVariables = {

};
window.tiktak = globalVariables;

window.runScript = (data, dev = false, timeline_point = "day", start_date = "", end_date = "", type = 1) => {
    //console.time("init");
    config['timeline_point'] = timeline_point;
    data_class.setResources(data.resources);
    data_class.setEvents(data.events);
    data_class.setSubResources(data.sub_resources);
    ReactDOM.render(<Main height="800" globalVariables={globalVariables} timeline_point={timeline_point}/>, document.getElementById(('root')));
};
// setInterval(() => {
//     console.log(window.tiktak);
// },1000);
// window.tiktak.onBeforeDrag = (e) => {
//
// };
//
window.runScript({
    events: events,
    resources: resources,
    sub_resources: sub_resources
});
