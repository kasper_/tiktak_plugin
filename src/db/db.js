export const events = {
    "14": {
        "id": "14",
        "name": "Player",
        "start": "2018-07-08 09:01:14",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7",
        "sub_tasks": [
            {
                "id": "58",
                "name": "skin",
                "start": "2018-07-11 13:57:10",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "0",
                "checked": "1"
            },
            {
                "id": "59",
                "name": "fullscreen",
                "start": "2018-07-11 13:57:15",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "0",
                "checked": "1"
            },
            {
                "id": "63",
                "name": "\u0412\u0438\u0439\u0442\u0438 \u0437 fullscreen \u043a\u043b\u0430\u0432\u0456\u0448\u0430 \"ecs\"",
                "start": "2018-07-17 10:05:53",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "0",
                "checked": "1"
            },
            {
                "id": "60",
                "name": "volume",
                "start": "2018-07-11 13:57:20",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "0",
                "checked": "0"
            },
            {
                "id": "61",
                "name": "userClose",
                "start": "2018-07-11 13:57:40",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "0",
                "checked": "1"
            }
        ]
    },
    "15": {
        "id": "15",
        "name": "booster (queue based)",
        "start": "2018-07-08 09:10:23",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "16": {
        "id": "16",
        "name": "white ads with async parse (queue based)",
        "start": "2018-07-08 09:11:35",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "17": {
        "id": "17",
        "name": "RTB VPAID",
        "start": "2018-07-08 09:12:40",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "19": {
        "id": "19",
        "name": "Skip by timing (check)",
        "start": "2018-07-08 09:50:37",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7",
        "sub_tasks": [
            {
                "id": "20",
                "name": "time to AdImpression",
                "start": "2018-07-08 09:50:58",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "7",
                "checked": "0"
            },
            {
                "id": "21",
                "name": "time to AdStopped",
                "start": "2018-07-08 09:51:15",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "7",
                "checked": "0"
            },
            {
                "id": "22",
                "name": "request timeout (pending)",
                "start": "2018-07-08 09:54:28",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "7",
                "checked": "0"
            },
            {
                "id": "23",
                "name": "time between AdVideoComplete and AdImpression or AdStopped",
                "start": "2018-07-08 09:55:46",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "7",
                "checked": "0"
            }
        ]
    },
    "20": {
        "id": "20",
        "name": "time to AdImpression",
        "start": "2018-07-08 09:50:58",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "21": {
        "id": "21",
        "name": "time to AdStopped",
        "start": "2018-07-08 09:51:15",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "22": {
        "id": "22",
        "name": "request timeout (pending)",
        "start": "2018-07-08 09:54:28",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "23": {
        "id": "23",
        "name": "time between AdVideoComplete and AdImpression or AdStopped",
        "start": "2018-07-08 09:55:46",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "25": {
        "id": "25",
        "name": "Alerts",
        "start": "2018-07-08 09:57:23",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7",
        "sub_tasks": [
            {
                "id": "26",
                "name": "DB INSERT checker",
                "start": "2018-07-08 09:58:28",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "7",
                "checked": "0"
            },
            {
                "id": "27",
                "name": "status code checker",
                "start": "2018-07-08 09:57:49",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "7",
                "checked": "0"
            },
            {
                "id": "28",
                "name": "embed.js",
                "start": "2018-07-08 09:59:10",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "7",
                "checked": "0"
            },
            {
                "id": "29",
                "name": "vpaid.js",
                "start": "2018-07-08 09:59:26",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "7",
                "checked": "0"
            },
            {
                "id": "30",
                "name": "iframe.html",
                "start": "2018-07-08 09:59:35",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "7",
                "checked": "0"
            },
            {
                "id": "31",
                "name": "zetcat.com",
                "start": "2018-07-08 09:59:50",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "7",
                "checked": "0"
            },
            {
                "id": "32",
                "name": "3647.tech",
                "start": "2018-07-08 10:00:18",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "7",
                "checked": "0"
            },
            {
                "id": "33",
                "name": "Real Time analytics",
                "start": "2018-07-08 10:01:24",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "7",
                "checked": "0"
            },
            {
                "id": "34",
                "name": "errors",
                "start": "2018-07-08 10:01:52",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "7",
                "checked": "0"
            },
            {
                "id": "35",
                "name": "set-full-ad-list",
                "start": "2018-07-08 10:09:04",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "7",
                "checked": "0"
            },
            {
                "id": "36",
                "name": "event tracking",
                "start": "2018-07-08 10:10:02",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "7",
                "checked": "0"
            },
            {
                "id": "41",
                "name": "white requests",
                "start": "2018-07-08 13:47:19",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "7",
                "checked": "0"
            },
            {
                "id": "43",
                "name": "white impressions",
                "start": "2018-07-08 13:47:38",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "7",
                "checked": "0"
            },
            {
                "id": "42",
                "name": "booster requests",
                "start": "2018-07-08 13:47:31",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "7",
                "checked": "0"
            },
            {
                "id": "44",
                "name": "booster impressions",
                "start": "2018-07-08 13:47:46",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "7",
                "checked": "0"
            },
            {
                "id": "37",
                "name": "http\/https checker",
                "start": "2018-07-08 09:57:37",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "7",
                "checked": "0"
            },
            {
                "id": "38",
                "name": "zetcat.com",
                "start": "2018-07-08 10:11:04",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "7",
                "checked": "0"
            },
            {
                "id": "39",
                "name": "3647.tech",
                "start": "2018-07-08 10:11:10",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "7",
                "checked": "0"
            },
            {
                "id": "40",
                "name": "GA sessions",
                "start": "2018-07-08 10:12:15",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "7",
                "checked": "0"
            }
        ]
    },
    "26": {
        "id": "26",
        "name": "DB INSERT checker",
        "start": "2018-07-08 09:58:28",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "27": {
        "id": "27",
        "name": "status code checker",
        "start": "2018-07-08 09:57:49",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "28": {
        "id": "28",
        "name": "embed.js",
        "start": "2018-07-08 09:59:10",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "29": {
        "id": "29",
        "name": "vpaid.js",
        "start": "2018-07-08 09:59:26",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "30": {
        "id": "30",
        "name": "iframe.html",
        "start": "2018-07-08 09:59:35",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "31": {
        "id": "31",
        "name": "zetcat.com",
        "start": "2018-07-08 09:59:50",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "32": {
        "id": "32",
        "name": "3647.tech",
        "start": "2018-07-08 10:00:18",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "33": {
        "id": "33",
        "name": "Real Time analytics",
        "start": "2018-07-08 10:01:24",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "34": {
        "id": "34",
        "name": "errors",
        "start": "2018-07-08 10:01:52",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "35": {
        "id": "35",
        "name": "set-full-ad-list",
        "start": "2018-07-08 10:09:04",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "36": {
        "id": "36",
        "name": "event tracking",
        "start": "2018-07-08 10:10:02",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "37": {
        "id": "37",
        "name": "http\/https checker",
        "start": "2018-07-08 09:57:37",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "38": {
        "id": "38",
        "name": "zetcat.com",
        "start": "2018-07-08 10:11:04",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "39": {
        "id": "39",
        "name": "3647.tech",
        "start": "2018-07-08 10:11:10",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "40": {
        "id": "40",
        "name": "GA sessions",
        "start": "2018-07-08 10:12:15",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "41": {
        "id": "41",
        "name": "white requests",
        "start": "2018-07-08 13:47:19",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "42": {
        "id": "42",
        "name": "booster requests",
        "start": "2018-07-08 13:47:31",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "43": {
        "id": "43",
        "name": "white impressions",
        "start": "2018-07-08 13:47:38",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "44": {
        "id": "44",
        "name": "booster impressions",
        "start": "2018-07-08 13:47:46",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "45": {
        "id": "45",
        "name": "VAST extension (\u0417\u0430\u0431\u0440\u0430\u0442\u0438 \u043a\u0435\u0440\u0443\u0432\u0430\u043d\u043d\u044f \u0437\u0432\u0443\u043a\u043e\u043c \u0432 \u0434\u0435\u044f\u043a\u0438\u0445 VPAID)",
        "start": "2018-07-09 07:21:37",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "46": {
        "id": "46",
        "name": "\u041f\u0440\u043e\u0431\u043b\u0435\u043c\u0430 I\/O disk space \u043d\u0430 \u0441\u0435\u0440\u0432\u0435\u0440\u0430\u0445 \u0441\u0442\u0430\u0442\u0438\u0441\u0442\u0438\u043a\u0438",
        "start": "2018-07-09 16:01:15",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "8",
        "sub_tasks": [
            {
                "id": "47",
                "name": "\u0441\u0440\u0430\u0432\u043d\u0438\u0442\u044c \u0441 \u0430\u043d\u0430\u043b\u043e\u0433\u0438\u0447\u043d\u044b\u043c \u0441\u0435\u0440\u0432\u0435\u0440\u043e\u043c",
                "start": "2018-07-09 16:02:18",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "8",
                "checked": "1"
            },
            {
                "id": "48",
                "name": "\u0432\u044b\u044f\u0432\u0438\u0442\u044c \u043f\u0440\u043e\u0431\u043b\u0435\u043c\u0443",
                "start": "2018-07-09 16:02:50",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "8",
                "checked": "1"
            },
            {
                "id": "54",
                "name": "\u0438\u0441\u043a\u043b\u044e\u0447\u0438\u0442\u044c \u0432\u0430\u0440\u0438\u0430\u043d\u0442 \u043e\u0448\u0438\u0431\u043a\u0438 \u0432 \u043a\u043e\u0434\u0435",
                "start": "2018-07-09 16:07:35",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "12",
                "checked": "1"
            },
            {
                "id": "49",
                "name": "\u041f\u043e\u0434\u043d\u044f\u0442\u044c \u0442\u0435\u0441\u0442\u043e\u0432\u044b\u0439 \u0441\u0435\u0440\u0432\u0435\u0440 \u0434\u043b\u044f \u0440\u0435\u0448\u0435\u043d\u0438\u044f \u0433\u0438\u043f\u043e\u0442\u0435\u0437\u044b \u043f\u0440\u043e\u0431\u043b\u0435\u043c\u044b \u0432\u0435\u0440\u0441\u0438\u0438 lsphp",
                "start": "2018-07-09 16:03:54",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "8",
                "checked": "0"
            },
            {
                "id": "50",
                "name": "\u0440\u0430\u0437\u0432\u0435\u0440\u043d\u0443\u0442\u044c \u0447\u0438\u0441\u0442\u0443\u044e \u0411\u0414 \u0441\u0442\u0430\u0442\u044b",
                "start": "2018-07-09 16:04:16",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "8",
                "checked": "1"
            },
            {
                "id": "51",
                "name": "\u0440\u0430\u0437\u0432\u0435\u0440\u043d\u0443\u0442\u044c \u0432\u0435\u0440\u0441\u0438\u0438\u044e lsphp70",
                "start": "2018-07-09 16:05:26",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "8",
                "checked": "1"
            },
            {
                "id": "57",
                "name": "\u043e\u0447\u0438\u0441\u0442\u0438\u0442\u044c \u0411\u0414 \u0441\u0442\u0430\u0442\u044b",
                "start": "2018-07-10 01:49:21",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "12",
                "checked": "1"
            },
            {
                "id": "52",
                "name": "\u0414\u043e\u0431\u0430\u0432\u0438\u0442\u044c \u0441\u0435\u0440\u0432\u0435\u0440 \u0432 \u0441\u0442\u0435\u043a",
                "start": "2018-07-09 16:06:03",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "12",
                "checked": "1"
            },
            {
                "id": "53",
                "name": "\u043f\u0440\u043e\u0442\u0435\u0441\u0442\u0438\u0440\u043e\u0432\u0430\u0442\u044c \u043f\u043e\u0432\u0435\u0434\u0435\u043d\u0438\u0435 \u0441\u0435\u0440\u0432\u0435\u0440\u0430 \u0432 \u0442\u0435\u0447\u0435\u043d\u0438\u0438 \u0434\u043d\u044f - \u043f\u043e\u0441\u043c\u043e\u0442\u0440\u0435\u0442\u044c \u0432\u044b\u0432\u043e\u0434 iotop",
                "start": "2018-07-09 16:06:48",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "8",
                "checked": "1"
            },
            {
                "id": "55",
                "name": "\u0435\u0441\u043b\u0438 \u0441\u0435\u0440\u0432\u0435\u0440 \u0441\u0435\u0431\u044f \u0432\u0435\u0434\u0435\u0442 \u0430\u0434\u0435\u043a\u0432\u0430\u0442\u043d\u043e \u043f\u0435\u0440\u0435\u0432\u0435\u0441\u0442\u0438 \u0432\u0441\u0435 \u0441\u0435\u0440\u0432\u0435\u0440\u044b \u043f\u043e \u043e\u0447\u0435\u0440\u0435\u0434\u0438 \u043d\u0430 \u0432\u0435\u0440\u0441\u0438\u044e lsphp70",
                "start": "2018-07-09 16:08:52",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "8",
                "checked": "1"
            },
            {
                "id": "62",
                "name": "\u0441\u0435\u0440\u0432\u0435\u0440 \u0432\u0435\u0434\u0435\u0442 \u0441\u0435\u0431\u044f \u043d\u0435\u0430\u0434\u0435\u043a\u0432\u0430\u0442\u043d\u043e \u043e\u0431\u043d\u0430\u0440\u0443\u0436\u0435\u043d \u0431\u0430\u0433 \u0432 \u043f\u0440\u0438\u043b\u043e\u0436\u0435\u043d\u0438\u0438 (\u0441\u043c. \u043a\u043e\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439) \u043d\u0435\u043e\u0431\u0445\u043e\u0434\u0438\u043c\u043e \u0435\u0433\u043e \u0438\u0441\u043f\u0440\u0430\u0432\u0438\u0442\u044c",
                "start": "2018-07-16 12:04:08",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "12",
                "checked": "0"
            }
        ]
    },
    "47": {
        "id": "47",
        "name": "\u0441\u0440\u0430\u0432\u043d\u0438\u0442\u044c \u0441 \u0430\u043d\u0430\u043b\u043e\u0433\u0438\u0447\u043d\u044b\u043c \u0441\u0435\u0440\u0432\u0435\u0440\u043e\u043c",
        "start": "2018-07-09 16:02:18",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "8"
    },
    "48": {
        "id": "48",
        "name": "\u0432\u044b\u044f\u0432\u0438\u0442\u044c \u043f\u0440\u043e\u0431\u043b\u0435\u043c\u0443",
        "start": "2018-07-09 16:02:50",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "8"
    },
    "49": {
        "id": "49",
        "name": "\u041f\u043e\u0434\u043d\u044f\u0442\u044c \u0442\u0435\u0441\u0442\u043e\u0432\u044b\u0439 \u0441\u0435\u0440\u0432\u0435\u0440 \u0434\u043b\u044f \u0440\u0435\u0448\u0435\u043d\u0438\u044f \u0433\u0438\u043f\u043e\u0442\u0435\u0437\u044b \u043f\u0440\u043e\u0431\u043b\u0435\u043c\u044b \u0432\u0435\u0440\u0441\u0438\u0438 lsphp",
        "start": "2018-07-09 16:03:54",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "8"
    },
    "50": {
        "id": "50",
        "name": "\u0440\u0430\u0437\u0432\u0435\u0440\u043d\u0443\u0442\u044c \u0447\u0438\u0441\u0442\u0443\u044e \u0411\u0414 \u0441\u0442\u0430\u0442\u044b",
        "start": "2018-07-09 16:04:16",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "8"
    },
    "51": {
        "id": "51",
        "name": "\u0440\u0430\u0437\u0432\u0435\u0440\u043d\u0443\u0442\u044c \u0432\u0435\u0440\u0441\u0438\u0438\u044e lsphp70",
        "start": "2018-07-09 16:05:26",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "8"
    },
    "52": {
        "id": "52",
        "name": "\u0414\u043e\u0431\u0430\u0432\u0438\u0442\u044c \u0441\u0435\u0440\u0432\u0435\u0440 \u0432 \u0441\u0442\u0435\u043a",
        "start": "2018-07-09 16:06:03",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "12"
    },
    "53": {
        "id": "53",
        "name": "\u043f\u0440\u043e\u0442\u0435\u0441\u0442\u0438\u0440\u043e\u0432\u0430\u0442\u044c \u043f\u043e\u0432\u0435\u0434\u0435\u043d\u0438\u0435 \u0441\u0435\u0440\u0432\u0435\u0440\u0430 \u0432 \u0442\u0435\u0447\u0435\u043d\u0438\u0438 \u0434\u043d\u044f - \u043f\u043e\u0441\u043c\u043e\u0442\u0440\u0435\u0442\u044c \u0432\u044b\u0432\u043e\u0434 iotop",
        "start": "2018-07-09 16:06:48",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "8"
    },
    "54": {
        "id": "54",
        "name": "\u0438\u0441\u043a\u043b\u044e\u0447\u0438\u0442\u044c \u0432\u0430\u0440\u0438\u0430\u043d\u0442 \u043e\u0448\u0438\u0431\u043a\u0438 \u0432 \u043a\u043e\u0434\u0435",
        "start": "2018-07-09 16:07:35",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "12"
    },
    "55": {
        "id": "55",
        "name": "\u0435\u0441\u043b\u0438 \u0441\u0435\u0440\u0432\u0435\u0440 \u0441\u0435\u0431\u044f \u0432\u0435\u0434\u0435\u0442 \u0430\u0434\u0435\u043a\u0432\u0430\u0442\u043d\u043e \u043f\u0435\u0440\u0435\u0432\u0435\u0441\u0442\u0438 \u0432\u0441\u0435 \u0441\u0435\u0440\u0432\u0435\u0440\u044b \u043f\u043e \u043e\u0447\u0435\u0440\u0435\u0434\u0438 \u043d\u0430 \u0432\u0435\u0440\u0441\u0438\u044e lsphp70",
        "start": "2018-07-09 16:08:52",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "8"
    },
    "56": {
        "id": "56",
        "name": "GO2NET VPAID temporary wrapper",
        "start": "2018-07-09 18:41:20",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "57": {
        "id": "57",
        "name": "\u043e\u0447\u0438\u0441\u0442\u0438\u0442\u044c \u0411\u0414 \u0441\u0442\u0430\u0442\u044b",
        "start": "2018-07-10 01:49:21",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "12"
    },
    "58": {
        "id": "58",
        "name": "skin",
        "start": "2018-07-11 13:57:10",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "0"
    },
    "59": {
        "id": "59",
        "name": "fullscreen",
        "start": "2018-07-11 13:57:15",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "0"
    },
    "60": {
        "id": "60",
        "name": "volume",
        "start": "2018-07-11 13:57:20",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "0"
    },
    "61": {
        "id": "61",
        "name": "userClose",
        "start": "2018-07-11 13:57:40",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "0"
    },
    "62": {
        "id": "62",
        "name": "\u0441\u0435\u0440\u0432\u0435\u0440 \u0432\u0435\u0434\u0435\u0442 \u0441\u0435\u0431\u044f \u043d\u0435\u0430\u0434\u0435\u043a\u0432\u0430\u0442\u043d\u043e \u043e\u0431\u043d\u0430\u0440\u0443\u0436\u0435\u043d \u0431\u0430\u0433 \u0432 \u043f\u0440\u0438\u043b\u043e\u0436\u0435\u043d\u0438\u0438 (\u0441\u043c. \u043a\u043e\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439) \u043d\u0435\u043e\u0431\u0445\u043e\u0434\u0438\u043c\u043e \u0435\u0433\u043e \u0438\u0441\u043f\u0440\u0430\u0432\u0438\u0442\u044c",
        "start": "2018-07-16 12:04:08",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "12"
    },
    "63": {
        "id": "63",
        "name": "\u0412\u0438\u0439\u0442\u0438 \u0437 fullscreen \u043a\u043b\u0430\u0432\u0456\u0448\u0430 \"ecs\"",
        "start": "2018-07-17 10:05:53",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "0"
    },
    "64": {
        "id": "64",
        "name": "\u0422\u0438\u043f \u0444\u0438\u043b\u044c\u0442\u0440\u0430\u0446\u0438\u0438 (\u043f\u0440\u0438\u043e\u0440\u0438\u0442\u0435\u0442 \u043d\u0430 \u0431\u0443\u0441\u0442\u0440)",
        "start": "2018-07-18 07:31:28",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "0",
        "sub_tasks": [
            {
                "id": "65",
                "name": "player",
                "start": "2018-07-18 07:33:52",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "7",
                "checked": "0"
            },
            {
                "id": "66",
                "name": "backend",
                "start": "2018-07-18 07:33:55",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "12",
                "checked": "1"
            }
        ]
    },
    "65": {
        "id": "65",
        "name": "player",
        "start": "2018-07-18 07:33:52",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "66": {
        "id": "66",
        "name": "backend",
        "start": "2018-07-18 07:33:55",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "12"
    },
    "67": {
        "id": "67",
        "name": "\u041f\u0438\u043a\u0441\u0435\u043b\u044c \u0442\u0435\u0445. \u0434\u043e\u043c\u0435\u043d\u0430 \u0441 API \u0434\u043b\u044f \u0440\u0430\u0431\u043e\u0442\u044b \u0441 cookie",
        "start": "2018-07-18 07:49:56",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "68": {
        "id": "68",
        "name": "\u041a\u043b\u0438\u043a\u0430\u043d\u0434\u0435\u0440",
        "start": "2018-07-18 08:04:04",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "69": {
        "id": "69",
        "name": "\u041a\u043b\u0438\u043a-\u0442\u0443-\u043f\u043b\u0435\u0439\u043d\u0430\u044f \u0438\u043d\u0438\u0446\u0438\u0430\u043b\u0438\u0437\u0430\u0446\u0438\u044f \u0431\u0443\u0441\u0442\u0435\u0440\u043e\u0432",
        "start": "2018-07-18 08:06:49",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "70": {
        "id": "70",
        "name": "\u0414\u043b\u044f \u0443\u0432\u0435\u043b\u0438\u0447\u0435\u043d\u0438\u044f \u0447\u0438\u0441\u043b\u0430 \u043f\u043e\u043a\u0430\u0437\u043e\u0432 \u043d\u0435\u043e\u0431\u0445\u043e\u0434\u0438\u043c \u0441\u0438\u043d\u043a REPUBLER",
        "start": "2018-07-20 09:31:43",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "71": {
        "id": "71",
        "name": "\u041f\u0440\u043e\u043a\u043b\u0430\u0434\u043a\u0438 ZC (go2net)",
        "start": "2018-07-26 09:07:13",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "72": {
        "id": "72",
        "name": "\u0417\u0430\u0449\u0438\u0442\u0430 \u043e\u0442 \u043d\u0430\u043a\u0440\u0443\u0442\u043e\u043a \u0432\u0430\u0441\u0442\u0430",
        "start": "2018-08-02 06:47:00",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "12"
    },
    "73": {
        "id": "73",
        "name": "\u0417\u043d\u0430\u0445\u043e\u0434\u0438\u0442\u0438 \u0442\u0430 \u0431\u043b\u043e\u043a\u0443\u0432\u0430\u0442\u0438 \u043d\u0430\u0448\u0443 \u0440\u0435\u043a\u043b\u0430\u043c\u0443 \u0432 \u043d\u0430\u0448\u0438\u0445 \u0440\u0435\u043a\u043b\u0456\u0432",
        "start": "2018-08-02 07:02:42",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "74": {
        "id": "74",
        "name": "\u041f\u0440\u043e\u0431\u043b\u0435\u043c\u0430 \u0437 \u0435\u043c\u0431\u0435\u0434\u043e\u043c",
        "start": "2018-08-05 09:27:13",
        "end": "",
        "resource": "9",
        "color": "#fdf093",
        "responsible_uid": "7"
    },
    "75": {
        "id": "75",
        "name": "\u043e\u043f\u0442\u0438\u043c\u0430\u043b\u044c\u043d\u0438\u0439 cdn \u0434\u043b\u044f \u0440\u043e\u0437\u0434\u0430\u0447\u0456",
        "start": "2018-07-08 09:03:23",
        "end": "",
        "resource": "11",
        "color": "#fdf093",
        "responsible_uid": "8",
        "sub_tasks": [
            {
                "id": "77",
                "name": "\u0432\u0438\u0431\u0440\u0430\u0442\u0438 15 \u043d\u0430\u0439\u043a\u0440\u0430\u0449\u0438\u0445 \u043a\u043e\u043d\u043a\u0443\u0440\u0435\u043d\u0442\u0456\u0432 \u0432 \u0434\u0430\u043d\u0456\u0439 \u0441\u0444\u0435\u0440\u0456",
                "start": "2018-07-08 11:23:10",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "8",
                "checked": "1"
            },
            {
                "id": "78",
                "name": "\u0432\u0438\u0447\u0438\u0441\u043b\u0438\u0442\u0438 \u0457\u0445 cdn \u0441\u0435\u0440\u0432\u0456\u0441 \/\u0445\u043e\u0441\u0442\u0435\u0440\u0430",
                "start": "2018-07-08 11:23:34",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "8",
                "checked": "1"
            },
            {
                "id": "79",
                "name": "\u043d\u0430\u043f\u0438\u0441\u0430\u0442\u0438 \u043b\u0438\u0441\u0442\u0430 \u0434\u0430\u043d\u0438\u043c \u0441\u0435\u0440\u0432\u0456\u0441\u0430\u043c \u0437 \u0437\u0430\u043f\u0438\u0442\u043e\u043c \u043d\u0430 \u0446\u0456\u043d\u0443, \u0433\u0435\u043e\u0440\u043e\u0437\u043c\u0456\u0449\u0435\u043d\u043d\u044f, \u0442\u0438\u043f \u043e\u043f\u043b\u0430\u0442\u0438, \u0430\u0431\u0443\u0437\u043e\u0441\u0442\u0456\u0439\u043a\u0456\u0441\u0442\u044c, \u0442\u0438\u043f \u0440\u043e\u0437\u0434\u0430\u0447\u0456 \u043a\u043e\u043d\u0442\u0435\u043d\u0442\u0430 - cdn \u043a\u0435\u0448\u0443\u044e\u0447\u0438\u0439 \u0447\u0438 \u0437\u0431\u0435\u0440\u0456\u0433\u0430\u044e\u0447\u0438\u0439, \u0447\u0438 \u0432\u0430\u0437\u0430\u0433\u043b\u0456 \u043d\u0430\u0434\u0430\u044e\u0442\u044c \u043f\u043e\u0441\u043b\u0443\u0433\u0438 cdn",
                "start": "2018-07-08 11:25:09",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "8",
                "checked": "1"
            },
            {
                "id": "80",
                "name": "\u0432\u0456\u0434\u0441\u0456\u044f\u0442\u0438 \u043d\u0430\u0439\u0431\u0456\u043b\u044c\u0448 \u0430\u0434\u0443\u043a\u0432\u0430\u0442\u043d\u0438\u0445 \u0432 \u0441\u043f\u0456\u0432\u0432\u0456\u0434\u043d\u043e\u0448\u0435\u043d\u043d\u0456 \u0446\u0456\u043d\u0430\/\u0430\u0431\u0443\u0437\u043e\u0441\u0442\u0456\u0439\u043a\u0456\u0441\u0442\u044c\/\u044f\u043a\u0456\u0441\u0442\u044c",
                "start": "2018-07-08 11:26:38",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "8",
                "checked": "1"
            },
            {
                "id": "81",
                "name": "\u043f\u0440\u043e\u0442\u0435\u0441\u0442\u0443\u0432\u0430\u0442\u0438 \u0441\u0435\u0440\u0432\u0456\u0441 Mnogobyte",
                "start": "2018-07-08 11:27:06",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "8",
                "checked": "0"
            },
            {
                "id": "99",
                "name": "\u043f\u0440\u043e\u0442\u0435\u0441\u0442\u0443\u0432\u0430\u0442\u0438 cdn",
                "start": "2018-07-10 01:53:37",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "11",
                "checked": "1"
            },
            {
                "id": "84",
                "name": "\u0427\u0435\u043a\u0430\u0454\u043c\u043e \u0440\u043e\u0437\u0448\u0438\u0440\u0435\u043d\u043d\u044f \u0444\u0443\u043d\u043a\u0446\u0456\u043e\u043d\u0430\u043b\u0443",
                "start": "2018-07-08 11:44:07",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "0",
                "checked": "0"
            },
            {
                "id": "82",
                "name": "\u041f\u0440\u043e\u0442\u0435\u0441\u0442\u0443\u0432\u0430\u0442\u0438 \u0441\u0435\u0440\u0432\u0456\u0441 integros",
                "start": "2018-07-08 11:27:22",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "0",
                "checked": "0"
            },
            {
                "id": "83",
                "name": "\u0427\u0435\u043a\u0430\u0454\u043c\u043e \u0457\u0445 \u0432\u0456\u0434\u043f\u043e\u0432\u0456\u0434\u0456",
                "start": "2018-07-08 11:43:43",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "0",
                "checked": "1"
            },
            {
                "id": "106",
                "name": "\u0432\u0441\u0442\u0430\u0432\u0438\u0442\u0438 \u0432 \u043f\u043b\u0435\u0454\u0440 \u0441\u0441\u0438\u043b\u043a\u0443 \u043d\u0430 cdn.on-kitchen.com",
                "start": "2018-07-11 06:40:54",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "11",
                "checked": "1"
            },
            {
                "id": "98",
                "name": "\u043f\u0440\u043e\u0442\u0435\u0441\u0442\u0443\u0432\u0430\u0442\u0438 \u0440\u043e\u0431\u043e\u0442\u0443 cdn",
                "start": "2018-07-10 01:53:00",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "11",
                "checked": "1"
            }
        ]
    },
    "76": {
        "id": "76",
        "name": "\u043e\u043f\u0442\u0438\u043c\u0430\u043b\u044c\u043d\u0430 \u0430\u0440\u0445\u0456\u0442\u0435\u043a\u0442\u0443\u0440\u0430 \u043f\u0456\u0434 \u0441\u0435\u0440\u0432\u0435\u0440\u0438 \u0440\u043e\u0437\u0434\u0430\u0447\u0456",
        "start": "2018-07-08 09:16:46",
        "end": "",
        "resource": "11",
        "color": "#fdf093",
        "responsible_uid": "8",
        "sub_tasks": [
            {
                "id": "85",
                "name": "\u0437\u043d\u0430\u0439\u0442\u0438 \u041f\u041e \u044f\u043a\u0435 \u0432\u043c\u0456\u0454 \u043a\u0435\u0448\u0443\u0432\u0430\u0442\u0438 \u0437\u0430\u043f\u0438\u0442\u0438 \u0434\u043e \u0432\u0430\u0440\u043d\u0456\u0448\u0456\u0432",
                "start": "2018-07-08 11:46:40",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "0",
                "checked": "0"
            },
            {
                "id": "86",
                "name": "\u0432\u0441\u0442\u0430\u043d\u043e\u0432\u0438\u0442\u0438 \u043d\u0430\u043b\u0430\u0448\u0442\u0443\u0432\u0430\u0442\u0438",
                "start": "2018-07-08 11:47:19",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "0",
                "checked": "0"
            },
            {
                "id": "87",
                "name": "\u0412\u0456\u0434\u0442\u0435\u0441\u0442\u0438\u0442\u0438 \u0440\u043e\u0431\u043e\u0442\u0443",
                "start": "2018-07-08 11:47:33",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "0",
                "checked": "0"
            }
        ]
    },
    "77": {
        "id": "77",
        "name": "\u0432\u0438\u0431\u0440\u0430\u0442\u0438 15 \u043d\u0430\u0439\u043a\u0440\u0430\u0449\u0438\u0445 \u043a\u043e\u043d\u043a\u0443\u0440\u0435\u043d\u0442\u0456\u0432 \u0432 \u0434\u0430\u043d\u0456\u0439 \u0441\u0444\u0435\u0440\u0456",
        "start": "2018-07-08 11:23:10",
        "end": "",
        "resource": "11",
        "color": "#fdf093",
        "responsible_uid": "8"
    },
    "78": {
        "id": "78",
        "name": "\u0432\u0438\u0447\u0438\u0441\u043b\u0438\u0442\u0438 \u0457\u0445 cdn \u0441\u0435\u0440\u0432\u0456\u0441 \/\u0445\u043e\u0441\u0442\u0435\u0440\u0430",
        "start": "2018-07-08 11:23:34",
        "end": "",
        "resource": "11",
        "color": "#fdf093",
        "responsible_uid": "8"
    },
    "79": {
        "id": "79",
        "name": "\u043d\u0430\u043f\u0438\u0441\u0430\u0442\u0438 \u043b\u0438\u0441\u0442\u0430 \u0434\u0430\u043d\u0438\u043c \u0441\u0435\u0440\u0432\u0456\u0441\u0430\u043c \u0437 \u0437\u0430\u043f\u0438\u0442\u043e\u043c \u043d\u0430 \u0446\u0456\u043d\u0443, \u0433\u0435\u043e\u0440\u043e\u0437\u043c\u0456\u0449\u0435\u043d\u043d\u044f, \u0442\u0438\u043f \u043e\u043f\u043b\u0430\u0442\u0438, \u0430\u0431\u0443\u0437\u043e\u0441\u0442\u0456\u0439\u043a\u0456\u0441\u0442\u044c, \u0442\u0438\u043f \u0440\u043e\u0437\u0434\u0430\u0447\u0456 \u043a\u043e\u043d\u0442\u0435\u043d\u0442\u0430 - cdn \u043a\u0435\u0448\u0443\u044e\u0447\u0438\u0439 \u0447\u0438 \u0437\u0431\u0435\u0440\u0456\u0433\u0430\u044e\u0447\u0438\u0439, \u0447\u0438 \u0432\u0430\u0437\u0430\u0433\u043b\u0456 \u043d\u0430\u0434\u0430\u044e\u0442\u044c \u043f\u043e\u0441\u043b\u0443\u0433\u0438 cdn",
        "start": "2018-07-08 11:25:09",
        "end": "",
        "resource": "11",
        "color": "#fdf093",
        "responsible_uid": "8"
    },
    "80": {
        "id": "80",
        "name": "\u0432\u0456\u0434\u0441\u0456\u044f\u0442\u0438 \u043d\u0430\u0439\u0431\u0456\u043b\u044c\u0448 \u0430\u0434\u0443\u043a\u0432\u0430\u0442\u043d\u0438\u0445 \u0432 \u0441\u043f\u0456\u0432\u0432\u0456\u0434\u043d\u043e\u0448\u0435\u043d\u043d\u0456 \u0446\u0456\u043d\u0430\/\u0430\u0431\u0443\u0437\u043e\u0441\u0442\u0456\u0439\u043a\u0456\u0441\u0442\u044c\/\u044f\u043a\u0456\u0441\u0442\u044c",
        "start": "2018-07-08 11:26:38",
        "end": "",
        "resource": "11",
        "color": "#fdf093",
        "responsible_uid": "8"
    },
    "81": {
        "id": "81",
        "name": "\u043f\u0440\u043e\u0442\u0435\u0441\u0442\u0443\u0432\u0430\u0442\u0438 \u0441\u0435\u0440\u0432\u0456\u0441 Mnogobyte",
        "start": "2018-07-08 11:27:06",
        "end": "",
        "resource": "11",
        "color": "#fdf093",
        "responsible_uid": "8"
    },
    "82": {
        "id": "82",
        "name": "\u041f\u0440\u043e\u0442\u0435\u0441\u0442\u0443\u0432\u0430\u0442\u0438 \u0441\u0435\u0440\u0432\u0456\u0441 integros",
        "start": "2018-07-08 11:27:22",
        "end": "",
        "resource": "11",
        "color": "#fdf093",
        "responsible_uid": "0"
    },
    "83": {
        "id": "83",
        "name": "\u0427\u0435\u043a\u0430\u0454\u043c\u043e \u0457\u0445 \u0432\u0456\u0434\u043f\u043e\u0432\u0456\u0434\u0456",
        "start": "2018-07-08 11:43:43",
        "end": "",
        "resource": "11",
        "color": "#fdf093",
        "responsible_uid": "0"
    },
    "84": {
        "id": "84",
        "name": "\u0427\u0435\u043a\u0430\u0454\u043c\u043e \u0440\u043e\u0437\u0448\u0438\u0440\u0435\u043d\u043d\u044f \u0444\u0443\u043d\u043a\u0446\u0456\u043e\u043d\u0430\u043b\u0443",
        "start": "2018-07-08 11:44:07",
        "end": "",
        "resource": "11",
        "color": "#fdf093",
        "responsible_uid": "0"
    },
    "85": {
        "id": "85",
        "name": "\u0437\u043d\u0430\u0439\u0442\u0438 \u041f\u041e \u044f\u043a\u0435 \u0432\u043c\u0456\u0454 \u043a\u0435\u0448\u0443\u0432\u0430\u0442\u0438 \u0437\u0430\u043f\u0438\u0442\u0438 \u0434\u043e \u0432\u0430\u0440\u043d\u0456\u0448\u0456\u0432",
        "start": "2018-07-08 11:46:40",
        "end": "",
        "resource": "11",
        "color": "#fdf093",
        "responsible_uid": "0"
    },
    "86": {
        "id": "86",
        "name": "\u0432\u0441\u0442\u0430\u043d\u043e\u0432\u0438\u0442\u0438 \u043d\u0430\u043b\u0430\u0448\u0442\u0443\u0432\u0430\u0442\u0438",
        "start": "2018-07-08 11:47:19",
        "end": "2018-07-09 11:47:19",
        "resource": "11",
        "color": "#fdf093",
        "responsible_uid": "8"
    },
    "87": {
        "id": "87",
        "name": "\u0412\u0456\u0434\u0442\u0435\u0441\u0442\u0438\u0442\u0438 \u0440\u043e\u0431\u043e\u0442\u0443",
        "start": "2018-07-08 11:47:33",
        "end": "",
        "resource": "11",
        "color": "#fdf093",
        "responsible_uid": "0"
    },
    "88": {
        "id": "88",
        "name": "\u043e\u043f\u0442\u0438\u043c\u0430\u043b\u044c\u043d\u0430 \u0441\u0445\u0435\u043c\u0430 \u0430\u043d\u0442\u0438\u0440\u043e\u0441\u043a\u043e\u043c\u043d\u0430\u0434\u0437\u043e\u0440\u0430",
        "start": "2018-07-08 12:11:26",
        "end": "",
        "resource": "12",
        "color": "#fdf093",
        "responsible_uid": "8",
        "sub_tasks": [
            {
                "id": "89",
                "name": "\u043f\u043e\u0448\u0443\u043a \u0430\u0431\u0443\u0437\u043e\u0441\u0442\u0456\u0439\u043a\u043e\u0433\u043e \u0445\u043e\u0441\u0442\u0435\u0440\u0430",
                "start": "2018-07-08 12:11:26",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "8",
                "checked": "0"
            },
            {
                "id": "90",
                "name": "\u043f\u0440\u043e\u0448\u0435\u0440\u0441\u0442\u0438\u0442\u0438 \u0444\u043e\u0440\u0443\u043c\u0438 \u0442\u0430 \u043f\u0440\u043e\u0433\u0443\u0433\u043b\u0438\u0442\u0438 \u0434\u0430\u043d\u0443 \u0442\u0435\u043c\u0443",
                "start": "2018-07-08 12:11:27",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "8",
                "checked": "1"
            },
            {
                "id": "91",
                "name": "\u0432\u0438\u0431\u0440\u0430\u0442\u0438 \u043d\u0430\u0439\u0431\u0456\u043b\u044c\u0448 \u043e\u043f\u0442\u0438\u043c\u0430\u043b\u044c\u043d\u0438\u0445 \u0442\u0430 \u0432\u0456\u0434\u0442\u0435\u0441\u0442\u0438\u0442\u0438",
                "start": "2018-07-08 12:11:27",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "8",
                "checked": "1"
            },
            {
                "id": "92",
                "name": "\u0432\u0438\u0431\u0456\u0440 \u0430\u0431\u0443\u0437\u043e\u0441\u0442\u0456\u0439\u043a\u043e\u0433\u043e \u0445\u043e\u0441\u0442\u0435\u0440\u0430",
                "start": "2018-07-08 12:11:27",
                "end": "",
                "color": "#ccc",
                "responsible_uid": "0",
                "checked": "0"
            }
        ]
    }
};

export const resource = {
    "9": {
        "id": "9",
        "name": "ZC",
        "color": "#fdf093",
        "children": [
            {
                "id": "7"
            },
            {
                "id": "8"
            },
            {
                "id": "12"
            }
        ]
    },
    "10": {
        "id": "10",
        "name": "Inbox",
        "color": "#fdf093",
        "general": "1"
    },
    "11": {
        "id": "11",
        "name": "VH",
        "color": "#fdf093",
        "general": "1",
        "children": [
            {
                "id": "8"
            },
            {
                "id": "11"
            }
        ]
    },
    "12": {
        "id": "12",
        "name": "Z\u043e\u043c\u0431\u0456",
        "color": "#fdf093",
        "children": [
            {
                "id": "4"
            },
            {
                "id": "5"
            },
            {
                "id": "8"
            },
            {
                "id": "10"
            },
            {
                "id": "11"
            },
            {
                "id": "14"
            },
            {
                "id": "16"
            }
        ]
    },
    "13": {
        "id": "13",
        "name": "GEEK",
        "color": "#fdf093"
    },
    "14": {
        "id": "14",
        "name": "TONEWS",
        "color": "#fdf093",
        "children": [
            {
                "id": "11"
            }
        ]
    },
    "15": {
        "id": "15",
        "name": "COMMON",
        "color": "#fdf093",
        "children": [
            {
                "id": "8"
            }
        ]
    },
    "16": {
        "id": "16",
        "name": "Team Inbox",
        "color": "red"
    },
    "17": {
        "id": "17",
        "name": "IMG",
        "color": "#fdf093"
    },
    "18": {
        "id": "18",
        "name": "tiktak plugin",
        "color": "#fdf093",
        "children": [
            {
                "id": "14"
            }
        ]
    }
};
export const sub_resources = {
    "3": {
        "id": "3",
        "name": "\u041e\u043b\u0435\u043a\u0441\u0430\u043d\u0434\u0440 \u041f\u043e\u0434\u0431\u043e\u0440\u0435\u0446\u043a\u0438\u0439"
    },
    "4": {
        "id": "4",
        "name": "\u0421\u0435\u0440\u0433\u0435\u0439 \u041f\u043e\u043f\u0435\u043d\u043a\u043e"
    },
    "5": {
        "id": "5",
        "name": "Roman Kutyk"
    },
    "6": {
        "id": "6",
        "name": "Vitaliy Mazur"
    },
    "7": {
        "id": "7",
        "name": "Oleksiy Fiks"
    },
    "8": {
        "id": "8",
        "name": "\u0410\u043d\u0430\u0442\u043e\u043b\u0438\u0439 \u0417\u0435\u043b\u0435\u043d\u043e\u0432\u0441\u043a\u0438\u0439"
    },
    "9": {
        "id": "9",
        "name": "Alina Kotova"
    },
    "10": {
        "id": "10",
        "name": "\u041d\u0438\u043a\u043e\u043b\u0430\u0439 \u041a\u043e\u0442"
    },
    "11": {
        "id": "11",
        "name": "\u0422\u0430\u0440\u0430\u0441 \u0414\u0440\u044f\u043d\u043d\u0438\u0445"
    },
    "12": {
        "id": "12",
        "name": "sakovecp@gmail.ru"
    },
    "13": {
        "id": "13",
        "name": "\u041d\u0430\u0442\u0430\u0448\u0430"
    },
    "14": {
        "id": "14",
        "name": "\u0410\u043d\u0434\u0440\u0456\u0439 \u041f\u0430\u043d\u0435\u043a\u0430\u043b\u043e"
    },
    "15": {
        "id": "15",
        "name": "\u0421\u0430\u0448\u0430 \u0412\u043b\u0430\u0441\u044e\u043a"
    },
    "16": {
        "id": "16",
        "name": "\u0410\u043d\u0434\u0440\u0456\u0439 \u0428\u0443\u043b\u044c\u0433\u0430\u0442\u0438\u0439"
    },
    "17": {
        "id": "17",
        "name": "\u041e\u043b\u0435\u0433 \u041d\u0435\u043c\u0443\u0440\u043e\u0432"
    },
    "18": {
        "id": "18",
        "name": "Dmitry"
    }
};
