import {config} from "../core/config";

export function getPosition(x,y){
    return {
        left: x*config.width_cell+x*2,
        top: y*config.height_cell+y*2
    };
}