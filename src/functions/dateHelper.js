import {config} from '../core/config'

export function getSubtract(DateStart, DateEnd) {
    let point = getOnePoint();
    let end = DateEnd.getTime()/(point);
    let start = DateStart.getTime()/(point);
    return Math.floor(end) - Math.floor(start);
}
export function getOnePoint(){
    let timeline_point = config.timeline_point;
    // console.log(JSON.stringify(config));
    let result = 1000 * 60 * 60 * 24;
    switch (timeline_point) {
        case 'day':
            result = 1000 * 60 * 60 * 24;
            break;
        case 'hour':
            result = 1000 * 60 * 60;
            break;
        case 'minute':
            result = 1000 * 60;
            break;
        default :
            break;
    }
    // console.log(result);
    return result;
}

export function getView(date) {
    let timeline_point = config.timeline_point;
    let view = date.getDate();
    switch (timeline_point) {
        case 'day':
            view = date.getDate();
            break;
        case 'hour':
            view = date.getHours();
            break;
        case 'minute':
            view = date.getMinutes();
            break;
        default :
            break;
    }
    return view;
}
// export function getAddSubstract(DateStart, Add) {
//     let timeline_point = TiktakComponent.onePointInMS === undefined ? 1000*60*60*24 : TiktakComponent.onePointInMS;
//     let start = DateStart.getTime()+(timeline_point * Add);
//     return new Date(start);
// }
export function format(date,type = 1) {
    let h,min,m,d,day,month,minute,hour,year;
    switch (type){
        case 1:
            h = date.getHours();
            min = date.getMinutes();
            m = date.getMonth();
            d = date.getDate();
            day = (d+1) < 10 ? "0"+(d+1) : "" + (d+1);
            month = (m+1) < 10 ? "0"+(m+1) : "" + (m+1);
            minute = (min+1) < 10 ? "0"+(min+1) : "" + (min+1);
            hour = (h+1) < 10 ? "0"+(h+1) : "" + (h+1);
            year = date.getFullYear();
            return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":00";
        case 2:
            h = date.getHours()-1;
            min = date.getMinutes()-1;
            m = date.getMonth();
            d = date.getDate()-1;
            day = (d+1) < 10 ? "0"+(d+1) : "" + (d+1);
            month = (m+1) < 10 ? "0"+(m+1) : "" + (m+1);
            minute = (min+1) < 10 ? "0"+(min+1) : "" + (min+1);
            hour = (h+1) < 10 ? "0"+(h+1) : "" + (h+1);
            year = date.getFullYear();
            return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":00";
        default:
            h = date.getHours();
            min = date.getMinutes();
            m = date.getMonth();
            d = date.getDate();
            day = (d+1) < 10 ? "0"+(d+1) : "" + (d+1);
            month = (m+1) < 10 ? "0"+(m+1) : "" + (m+1);
            minute = (min+1) < 10 ? "0"+(min+1) : "" + (min+1);
            hour = (h+1) < 10 ? "0"+(h+1) : "" + (h+1);
            year = date.getFullYear();
            return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":00";
    }
}