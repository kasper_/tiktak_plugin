import React from 'react'
import FormInput from "./form_elements/form_input";
import DateTime from "./form_elements/form_date_picker";
import FormTextArea from "./form_elements/form_textarea";
import FormTable from "./form_elements/form_table/index";
import FormSelect from './form_elements/form_select'
import FormCustomTable from "./form_elements/form_custom_table";

class TFormContext extends React.Component{

    state = {
        formValues: {}
    };

    onChange = (key, value) => {
        this.props.fields[key] = value;
        // console.log(this.props.fields);
    };

    addItem(value, key, params = {}, event = {}, onChange){
        // console.log(params);
        params['defaultValue'] = value;
        params['key'] = key + "_input";
        params['ref'] = key;
        switch (params["type"]){
            case 'text':
                return <FormInput key={key} value={value} name={key} params={params} event={event} onChange={onChange}/>;
            case 'textarea':
                return <FormTextArea key={key} value={value} name={key} params={params} event={event} onChange={onChange}/>;
            case 'date':
                return <DateTime key={key} value={value} name={key} params={params} event={event} onChange={onChange}/>;
            case 'table':
                return <FormTable key={key} value={value} name={key} params={params} event={event} visible={params['visible']}/>;
            case 'select':
                return <FormSelect key={key} value={value} name={key} params={params} list={this.props.sub_res} event={event} onChange={onChange}/>;
            case "custom-table":
                return <FormCustomTable key={key} value={value} name={key} params={params} onChange={onChange}/>;
            default:
                return <FormInput key={key} value={value} name={key} params={params} onChange={onChange}/>;
        }
    }

    render () {
        let data = this.props.data;
        let props = this.props.values;
        //console.log(data,props);
        let html = [];
        for (let key in data){
            if (props[key].active){
                html.push(this.addItem(data[key],key, props[key], data, this.onChange));
            }
        }
        return <div>{html}</div>;
    }
}

export default TFormContext;