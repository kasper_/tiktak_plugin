import React from 'react';
import './style.css';
import {config} from "../../../core/config";

class ItemHeader extends React.Component {

    state = {
        selected: false
    };

    style = {
        height: config.height_cell,
        width: config.width_cell
    };

    componentWillMount() {
        for (let key in this.props.style) {
            this.style[key] = this.props.style[key];
        }
        this.props.events['addStates'] = this.addStates;
    }

    componentWillUpdate() {
        this.props.events['addStates'] = this.addStates;
    }

    addStates = (states) => {
        if (Object.keys(states).length !== 0){
            this.setState(states);
        }
    };

    render () {
        let class_name = this.state.selected ? 'header-item-active' : 'header-item-de-active';

        return <div className={class_name} style={this.style} >{this.props.children}</div>;
    }

}

export default ItemHeader;