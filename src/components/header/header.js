import React from 'react'
import {getOnePoint, getView} from "../../functions/dateHelper";
import {config} from "../../core/config";
import data_class from "../../data_class";
import ItemHeader from "./item_header";

class Header extends React.Component {


    state = {
        startDate: this.props.start,
        endDate: this.props.end
    };

    init() {

    }

    componentWillMount() {
        this.html = [];
    }
    componentWillUpdate() {
        this.html = [];
    }
    render () {
        let points = data_class.points;
        let left = 0;
        let point = getOnePoint();
        // console.log(point);
        let start_time = this.state.startDate.getTime();
        // if (!this.props.data.hasOwnProperty('header')){
        //     this.props.data['header'] = {};
        // }
        for (let i = 0; i < points; i++) {
            let events = {};
            let date = new Date(start_time + i * point);
            left = i + config.width_cell * i;
            let top = 1;
            this.html.push(<ItemHeader key={"head_" + i} style={{
                position: 'absolute',
                left: left + "px",
                top: top + "px",
                textAlign: "center",
                userSelect: 'none'
            }} events={events} >{getView(date)}</ItemHeader>);

            this.props.data[i] = {
                values: {
                    date: date,
                    left: left
                },
                events: events
            };
        }
        return <div key="header" className="tiktak_header" style={{
            left: config.width_left_panel + "px",
            position: "absolute"
        }}>{this.html}</div>
    }
}

export default Header;