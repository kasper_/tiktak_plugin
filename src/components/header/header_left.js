import React from 'react'
import {config} from "../../core/config";

class HeaderLeft extends React.Component {

    render () {

        return <div key={"left_header"} style={{
            height: (config.height_cell) + "px",
            width: (config.width_left_panel - 2) + "px",
            backgroundColor: "#fff",
            position: "absolute",
            left: "1px",
            top: "1px"
        }}/>;
    }
}

export default HeaderLeft;