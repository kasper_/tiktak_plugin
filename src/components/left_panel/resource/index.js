import React from 'react';
import './style.css'
import {config} from "../../../core/config";
import TModalWindow from "../../tt_modal_window";

class Resource extends  React.Component {

    constructor(props) {
        super(props);

        this.onClick = this.onClick.bind(this);
        this.onDoubleClick = this.onDoubleClick.bind(this);
        this.state = {
            selected: false,
            active: false,
            update: true,
            modalShow: false,
            modalData: {},
            modalContent: null,
            isSubRes: props.is_sub_res,
            data: props.custom_data,
            element: props.custom_data.element,
            resource: props.resource,
        };
        this.fields = {};
        this.setFunctionToGlobalObject();
    }
    componentWillReceiveProps(props) {
        let state = {
            selected: false,
            active: false,
            update: true,
            // modalShow: false,
            // modalData: {},
            // modalContent: null,
            isSubRes: props.is_sub_res,
            data: props.custom_data,
            element: props.custom_data.element,
            resource: props.resource
        };
        this.setState(state);
        this.fields = {};

    }
    setFunctionToGlobalObject = () => {
        if (this.state.isSubRes) {
            if (!this.props.importFunctions.hasOwnProperty('childResource')) {
                this.props.importFunctions['childResource'] = {};
            }
            this.props.importFunctions['childResource'][this.state.resource.id + "_" + this.state.element.id] = this.reloadModal;
        } else {
            if (!this.props.importFunctions.hasOwnProperty('mainResource')) {
                this.props.importFunctions['mainResource'] = {};
            }
            this.props.importFunctions['mainResource'][this.state.element.id] = this.reloadModal;
        }
    };

    onDoubleClick = (e) => {

        if (this.state.modalShow) {
            return 0;
        }

        let type = this.state.isSubRes ? "child" : "main";
        let block = this.props.importFunctions['viewAllEventsInBlock'](type,this.state.element,this.state.resource);

        let modalValues = {};

        modalValues['custom-table'] = {
            onChange: this.openFormEvent,
            onCustom: "",
            type: "custom-table",
            active: true,
            label: ""
        };

        // console.log(event);
        this.fields = {};
        this.setState({
            modalShow: true,
            modalContent: "Table of events '" + this.props.custom_data.element.name + "'",
            modalData: {
                data: {
                    "custom-table": block.events
                },
                values: modalValues
            }
        });
    };

    reloadModal = () => {

        let type = this.state.isSubRes ? "child" : "main";
        let block = this.props.importFunctions['viewAllEventsInBlock'](type,this.state.element,this.state.resource);

        let modalValues = {};

        modalValues['custom-table'] = {
            onChange: this.openFormEvent,
            onCustom: "",
            type: "custom-table",
            active: true,
            label: ""
        };

        // console.log(event);
        this.fields = {};
        this.setState({
            modalShow: true,
            modalContent: "Table of events '" + this.props.custom_data.element.name + "'",
            modalData: {
                data: {
                    "custom-table": block.events
                },
                values: modalValues
            }
        });
    };

    openFormEvent = (e,event_id) => {
        if (!this.props.importFunctions.hasOwnProperty('eventAction')) {
            // console.log(this.props.importFunctions);
            return 0;
        }
        if (!this.props.importFunctions['eventAction'].hasOwnProperty(event_id)){
            // console.log(this.props.importFunctions['eventAction']);
            return 0;
        }
        this.props.importFunctions['eventAction'][[event_id]](e);
    };

    addState = (state) => {
        if (typeof state === 'object'){
            this.setState(state);
        }
    };

    onClick = () => {
        // this.setState({
        //     selected: !this.state.selected
        // })
        //this.props.action();
    };

    closeModal = () => {
        this.setState({modalShow: false});
    };

    onModalSave = (data) => {

    };

    onAction = (data) => {
        //console.log(data);
    };


    render () {
        //let top = this.props.values.bottom * config.height_cell * 2 + this.props.values.bottom * 2;
        let modal = <TModalWindow
            close={() => this.closeModal()}
            data={this.state.modalData.data}
            action={(data) => this.onAction(data)}
            values={this.state.modalData.values}
            sub_res={this.props.sub_res}
            type={1}>
            {this.state.modalContent}
        </TModalWindow>;

        this.props.custom_data['addState'] = this.addState;
        if (this.props.is_sub_res) {
            // console.log(top);
            let class_name = this.state.selected ? "sub-resource-active" : "sub-resource-de-active";
            //let subTop = (top+this.props.child_pos*2+this.props.child_pos) + "px";
            return <div
                onClick={this.onClick}
                className={class_name}
                style={{
                    position: 'absolute',
                    height: (2*config.height_cell+1) + "px",
                    width: (config.width_left_panel-52) + "px",
                    textAlign: 'center',
                    right: 0,
                    top: this.props.custom_data.top + 'px'
                }}
                onDoubleClick={(e) => this.onDoubleClick(e)}>{this.props.children}
                {this.state.modalShow && modal}</div>;
        } else {
            let class_name = this.state.selected ? "resource-active" : "resource-de-active";
            //console.log(this.props.values.bottom);
            return <div
                onClick={this.onClick}
                className={class_name}
                style={{
                    //top: top,
                    position: 'absolute',
                    height: (2*config.height_cell+1) + "px",
                    width: (config.width_left_panel-2) + "px",
                    textAlign: 'center',
                    top: this.props.custom_data.top + 'px'
                }}
                onDoubleClick={(e) => this.onDoubleClick(e)}
            >{this.props.children}
                {this.state.modalShow && modal}
            </div>;
        }
    }
}
export default Resource;