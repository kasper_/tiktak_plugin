import React from 'react';
import {config} from "../../core/config";
import Resource from "./resource";
//import GroupResource from "./group_resource/index";
//import {config} from "../../core/config";
//import {sub_resources} from "../../db/db";
//import {resource} from "../../db/db";
//import {config} from "../core/config";

class LeftPanel extends React.Component {

    state = {
        //resources: this.props.resources,
        update: true
    };

    // componentDidMount() {
    //     //console.log(this.props.data);
    //     console.log('test');
    // }

    componentWillMount() {
        //
        // this.html = html;
        // this.props.data.height = i*(config.height_cell*2+1)+(i);
    }

    componentWillUpdate() {

    }

    render () {

        let html = [];
        //let resources = [];
        let i = 0;
        // let custom_data = {};
        //
        //console.log(this.state.resources);
        for (let key in this.props.resources) {
            let resource = this.props.resources[key];
            let top = i*(config.height_cell*2+1)+(i);
            let custom_data = {
                element: resource,
                type: 'main',
                top: top
            };
            this.props.data[i] = custom_data;
            html.push(<Resource
                key={i}
                is_sub_res={false}
                custom_data={custom_data}
                resource={resource}
                sub_res={this.props.sub_res}
                importFunctions={this.props.importFunctions} >{resource.name}</Resource>);
            i++;
            if (resource.hasOwnProperty("children")) {
                for (let key in resource.children) {
                    // if (resource.id === "7") {
                    //     console.log(this.props.data[i]);
                    // }
                    let child = this.props.sub_res[resource.children[key].id];
                    // console.log(resource.children[key].id);
                    let top = i*(config.height_cell*2+1)+(i);
                    let custom_data = {
                        element: child,
                        type: 'child',
                        top: top,
                        main: {
                            id: resource.id
                        }
                    };
                    this.props.data[i] = custom_data;
                    html.push(<Resource
                        importFunctions={this.props.importFunctions}
                        key={i}
                        is_sub_res={true}
                        custom_data={custom_data}
                        child={child}
                        sub_res={this.props.sub_res}
                        resource={resource}
                    >{child.name}</Resource>);
                    i++;
                }
            }
        }
        //console.log(resources);
        return <div key={"left_panel"} style={this.props.style}>
                {html}
            </div>;
    }
}

export default LeftPanel;