import React from "react";
import TFormContext from "./tt_form_context"

const wrapperCss = {
    position: "fixed",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    backgroundColor: "rgba(0,0,0,0.8)",
    zIndex: 2
};

const css = {
    position: "relative",
    top: "10%",
    width: "300px",
    margin: "0 auto",
    padding: "10px",
    textAlign: "center",
    backgroundColor: "white",
    border: "1px solid blue",
    zIndex: 3
};

const TModalWindow = props => (
    <div style={wrapperCss}>
        <div style={css}>
            <div style={{textAlign: 'center'}}><span>{props.children}</span></div>
            <TFormContext data={props.data} values={props.values} sub_res={props.sub_res} fields={props.fields}/>
            <hr />
            {props.type === 2 &&
            (<button
                onClick={props.close}
                style={{ marginRight: "10px" }}>
                    Cancel
                </button>)}
            {props.type === 2 &&
            (<button
                onClick={() => {
                    props.action(props.data);
                    props.close();
                }}
            >Save</button>)}
            {props.type === 1 && (
                <button
                    onClick={props.close}
                >Close</button>
            )}
        </div>
    </div>
);


export default TModalWindow;