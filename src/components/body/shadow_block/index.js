import React from 'react';
import {config} from "../../../core/config";

class ShadowBlock extends React.Component {

    constructor (props) {
        super(props);

        this.state = {
            hidden: true,
            width: this.props.width+config.width_left_panel,
            height: this.props.height,
            topVisible: 0,
            heightVisible: 0,
            leftShadowBlock: -config.width_left_panel,
            zIndex: 1,
        };
        //

    }

    setStates = (states) => {
        this.setState(states);
    };

    render () {
        let styleShadow = {};
        let styleBefore = {};
        let styleAfter = {};
        // console.log(this.state);
        this.props.importFunctions.f = this.setStates;
        this.props.importFunctions.s = this.state;

        if (!this.state.hidden) {
            styleShadow['width'] = this.state.width + "px";
            styleShadow['height'] = this.state.height + "px";
            styleShadow['left'] = this.state.leftShadowBlock + "px";
            styleShadow['zIndex'] = this.state.zIndex;
            styleShadow['position'] = 'absolute';

            styleBefore['width'] = styleAfter['width'] = this.state.width + "px";
            styleBefore['backgroundColor'] = styleAfter['backgroundColor'] = '#fff';
            styleBefore['cursor'] = styleAfter['cursor'] = 'no-drop';
            styleBefore['position'] = styleAfter['position'] = 'absolute';
            styleBefore['zIndex'] = styleAfter['zIndex'] = 1;
            styleBefore['opacity'] = styleAfter['opacity'] = 0.9;
            styleBefore['filter'] = styleAfter['filter'] = 'blur(5px)contrast(.8)brightness(.8)';
            styleBefore['transmission'] = styleAfter['transmission'] = '.6s filter';
            styleBefore['OFilter'] = styleAfter['OFilter'] = 'blur(5px)contrast(.8)brightness(.8)';
            styleBefore['MsFilter'] = styleAfter['MsFilter'] = 'blur(5px)contrast(.8)brightness(.8)';
            styleBefore['MozFilter'] = styleAfter['MozFilter'] = 'blur(5px)contrast(.8)brightness(.8)';
            styleBefore['WebkitFilter'] = styleAfter['WebkitFilter'] = 'blur(5px)contrast(.8)brightness(.8)';

            styleBefore['left'] = styleAfter['left'] = 0;
            styleBefore['top'] = 0;
            styleAfter['top'] = (this.state.heightVisible + this.state.topVisible) + "px";
            styleBefore['height'] = this.state.topVisible + "px";
            styleAfter['height'] = (this.state.height - this.state.topVisible -  this.state.heightVisible) + "px";


            return <div style={styleShadow}>
                <div style={styleBefore}/>
                <div style={styleAfter}/>
            </div>;
        } else {

            return ""
        }
    }

}

export default ShadowBlock;