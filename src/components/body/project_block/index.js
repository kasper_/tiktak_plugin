import React from 'react';
import {config} from "../../../core/config";


class ProjectBlock extends React.Component {

    // constructor(props) {
    //     super(props);
    // }

    render () {
        //console.log("render");
        this.style = {
            position: 'absolute',
            height: (config.height_cell*2+1) + 'px'
        };
        if (typeof this.props.style === 'object') {
            for (let key in this.props.style) {
                this.style[key] = this.props.style[key];
            }
        }
        return <div style={this.style} />;
    }
}

export default ProjectBlock;