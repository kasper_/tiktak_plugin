import React from 'react';
import {config} from "../../../core/config";
import data_class from "../../../data_class";
import {getSubtract} from "../../../functions/dateHelper";
import TModalWindow from "../../tt_modal_window";
import './style.css';


class EventBlock extends React.Component {

    constructor(props) {
        super(props);
        //this.props.updateWithParent.value = false;


        this.state = {
            type: 0,
            event: props.event,
            modalShow: false,
            modalData: {},
            modalContent: null,
            position: 0,
            top: 0,
            left: 0,
            width: 0,
            zIndex: 0,
            edit: true,
            down: false,
            drag: false,
            resizeLeft: false,
            resizeLeftDown: false,
            resizeRight: false,
            resizeRightDown: false,
            dobleClick: false,
            resizeRightNum: 0,
            resizeLeftNum: 0
        };


        this.itemTypes = {
            name: {
                type: "textarea",
                label: "Text"
            },
            start: {
                type: "date",
                label: "Date add"
            },
            end: {
                type: "date",
                label: "Date end"
            },
            responsible_uid: {
                type: "select",
                label: "Select responsible user"
            },
            sub_tasks: {
                type: "table",
                label: "View all sub tasks"
            }
        };
        this.startX = 0;
        this.startY = 0;
        this.startTop = 0;
        this.startLeft = 0;

        // this.updatePosition();
        let isDate = false;
        let isResponsibile = false;
        //if (/\d{4}-\d{2}-\d{2}/.test(this.event.start) && /\d{4}-\d{2}-\d{2}/.test(this.event.end)) {
        if (data_class.isDate(this.state.event.start) && data_class.isDate(this.state.event.end)) {
            isDate = true;
        }
        if (this.state.event.responsible_uid !== "0" && this.state.event.responsible_uid !== 0 && this.state.event.responsible_uid !== null) {
            isResponsibile = true;
        }
        if (isDate && isResponsibile) {
            this.state['type'] = 1;
        } else if (!isResponsibile) {
            this.state['type'] = 3;
        } else if (!isDate && isResponsibile) {
            this.state['type'] = 2;
        }
        switch (this.state.type) {
            case 1:
                this.state.top = this.props.childBlocks[this.state.event.resource + "_" + this.state.event.responsible_uid].top;
                this.state.position = getSubtract(data_class.start_date, new Date(this.state.event.start));
                this.state.left = this.state.position * config.width_cell + this.state.position;
                let widthEvent = getSubtract(new Date(this.state.event.start), new Date(this.state.event.end));
                this.state.width = config.width_cell * (widthEvent+1) + widthEvent;
                break;
            case 2:
                if (this.props.childBlocks.hasOwnProperty(this.state.event.resource + "_" + this.state.event.responsible_uid)) {
                    this.state.position = this.props.childBlocks[this.state.event.resource + "_" + this.state.event.responsible_uid].left;
                    this.state.top = this.props.childBlocks[this.state.event.resource + "_" + this.state.event.responsible_uid].top + 1 + config.height_cell;
                    this.props.childBlocks[this.state.event.resource + "_" + this.state.event.responsible_uid].left = this.state.position + 1;
                    this.props.childBlocks[this.state.event.resource + "_" + this.state.event.responsible_uid].events[this.state.event.id] = this.state.event;
                    this.state.left = this.state.position * config.width_cell + this.state.position;
                }
                break;
            default:
                if (this.props.mainBlocks.hasOwnProperty(this.state.event.resource)) {
                    this.state.top = this.props.mainBlocks[this.state.event.resource].top;
                    this.state.position = this.props.mainBlocks[this.state.event.resource].left;
                    this.props.mainBlocks[this.state.event.resource].left = this.state.position + 1;
                    this.state.left = this.state.position * config.width_cell + this.state.position;
                    this.props.mainBlocks[this.state.event.resource].events[this.state.event.id] = this.state.event;
                }
                break;
        }
        this.Recived = false;
        this.props.eventStates['f'] = this.setStates.bind(this);
        //this.props.importFunctions['onMouseUp'] = this.onMouseUp;
        if (!props.importFunctions.hasOwnProperty('eventAction')) {
            props.importFunctions['eventAction'] = {};
        }
        props.importFunctions['eventAction'][props.event.id] = this.onDobleClick;
    }
    needUpdateComponent(oldState, state) {
        let update = false;
        for (let key in state) {
            if (state[key] !== 'object' && state[key] !== oldState[key]) {

                // this is all state dont reload component

                if (key === 'drag')
                {

                } else if (key === 'down') {

                } else if (key === 'dobleClick') {

                } else if (key === 'resizeRightDown') {

                } else if (key === 'resizeLeftDown') {

                } else {
                    update = true;
                }
            }
        }
        if (!update) {
            this.props.eventStates['s'] = state;
        }
        return update;
    }
    componentWillReceiveProps(props) {
        //console.log(props);
        let state = {
            noDrop: false,
            type: 0,
            event: props.event,
            position: 0,
            top: 0,
            left: 0,
            zIndex: 0,
            edit: true
        };
        this.Recived = true;
        // this.updatePosition();
        let isDate = false;
        let isResponsibile = false;
        //if (/\d{4}-\d{2}-\d{2}/.test(this.event.start) && /\d{4}-\d{2}-\d{2}/.test(this.event.end)) {
        if (data_class.isDate(state.event.start) && data_class.isDate(state.event.end)) {
            isDate = true;
        }
        if (state.event.responsible_uid !== "0" && state.event.responsible_uid !== 0 && state.event.responsible_uid !== null) {
            isResponsibile = true;
        }
        if (isDate && isResponsibile) {
            state['type'] = 1;
        } else if (!isResponsibile) {
            state['type'] = 3;
        } else if (!isDate && isResponsibile) {
            state['type'] = 2;
        }
        // console.log(this.state.event.start);
        // console.log(this.state.event.end);
        // console.log(this.state.event);
        switch (state.type) {
            case 1:
                if (!props.childBlocks.hasOwnProperty(state.event.resource + "_" + state.event.responsible_uid)){
                    props.childBlocks[state.event.resource + "_" + state.event.responsible_uid] = {};
                }

                state.top = props.childBlocks[state.event.resource + "_" + state.event.responsible_uid].top;
                state.position = getSubtract(data_class.start_date, new Date(state.event.start));
                state.left = state.position * config.width_cell + state.position;
                let widthEvent = getSubtract(new Date(state.event.start), new Date(state.event.end));
                state.width = config.width_cell * (widthEvent+1) + widthEvent;
                break;
            case 2:
                if (props.childBlocks.hasOwnProperty(state.event.resource + "_" + state.event.responsible_uid)) {
                    state.position = props.childBlocks[state.event.resource + "_" + state.event.responsible_uid].left;
                    state.top = props.childBlocks[state.event.resource + "_" + state.event.responsible_uid].top + 1 + config.height_cell;
                    props.childBlocks[state.event.resource + "_" + state.event.responsible_uid].left = state.position + 1;
                    props.childBlocks[state.event.resource + "_" + state.event.responsible_uid].events[state.event.id] = state.event;
                    state.left = state.position * config.width_cell + state.position;
                }
                break;
            default:
                if (props.mainBlocks.hasOwnProperty(state.event.resource)) {
                    state.top = props.mainBlocks[state.event.resource].top;
                    state.position = props.mainBlocks[state.event.resource].left;
                    props.mainBlocks[state.event.resource].left = state.position + 1;
                    state.left = state.position * config.width_cell + state.position;
                    props.mainBlocks[state.event.resource].events[state.event.id] = state.event;
                }
                break;
        }
        this.setState(state);
        if (!props.importFunctions.hasOwnProperty('eventAction')) {
            props.importFunctions['eventAction'] = {};
        }
        props.importFunctions['eventAction'][props.event.id] = this.onDobleClick;
    }
    // updateWithParent = this.props.updateWithParent;
    updatePosition = (event, constructor = true, advancedState = {}) => {
        let isDate = false;
        let isResponsibile = false;
        //if (/\d{4}-\d{2}-\d{2}/.test(this.event.start) && /\d{4}-\d{2}-\d{2}/.test(this.event.end)) {
        if (data_class.isDate(event.start) && data_class.isDate(event.end)) {
            isDate = true;
        }
        if (event.responsible_uid !== "0" && event.responsible_uid !== 0 && event.responsible_uid !== null) {
            isResponsibile = true;
        }
        let state = {};
        let event_ = event;
        let type = 0;
        if (isDate && isResponsibile) {
            type = 1;
        } else if (!isResponsibile) {
            type = 3;
        } else if (!isDate && isResponsibile) {
            type = 2;
        }
        let position = 0, top = 0, left = 0;
        switch (type) {
            case 1:
                top = this.props.childBlocks[event_.resource + "_" + event_.responsible_uid].top;
                position = getSubtract(data_class.start_date, new Date(event_.start));
                left = position * config.width_cell + position;
                let  widthEvent = getSubtract(new Date(event_.start), new Date(event_.end));
                state['width'] = config.width_cell * (widthEvent+1) + widthEvent;
                break;
            case 2:
                if (this.props.childBlocks.hasOwnProperty(event_.resource + "_" + event_.responsible_uid)) {
                    position = this.props.childBlocks[event_.resource + "_" + event_.responsible_uid].left;
                    top = this.props.childBlocks[event_.resource + "_" + event_.responsible_uid].top + 1 + config.height_cell;
                    this.props.childBlocks[event_.resource + "_" + event_.responsible_uid].left = position + 1;
                    this.props.childBlocks[event_.resource + "_" + event_.responsible_uid].events[event_.id] = event_;
                    left = position * config.width_cell + position;
                    this.props.operationWithBlock("child", event_, "add");
                    this.props.operationWithBlock("child", event_, "refresh");
                }
                break;
            default:
                if (this.props.mainBlocks.hasOwnProperty(event_.resource)) {
                    top = this.props.mainBlocks[event_.resource].top;
                    position = this.props.mainBlocks[event_.resource].left;
                    this.props.mainBlocks[event_.resource].left = position + 1;
                    left = position * config.width_cell + position;
                    this.props.mainBlocks[event_.resource].events[event_.id] = event;
                    this.props.operationWithBlock("main", event_, "add");
                    this.props.operationWithBlock("main", event_, "refresh");
                }
                break;
        }
        state['event'] = event_;
        state['top'] = top;
        state['left'] = left;
        state['position'] = position;
        state['type'] = type;

        for (let key in advancedState) {
            state[key] = advancedState[key];
        }
        this.setStates(state);
    };
    getLeftPx = (position) => {
        return position * config.width_cell + position;
    };
    setStates = (states) => {
        if (typeof states === 'object') {

            if (states.hasOwnProperty("position")) {
                states['left'] = this.getLeftPx(states.position);
                this.setState(states);
            } else {
                this.setState(states);
            }
        }
    };
    onClick = (event) => {

    };
    onDobleClick = (e = "") => {

        if (this.state.modalShow) {
            return 0;
        }
        this.setStates({
            dobleClick: true
        });
        let modalValues = {};
        let itemHaveSubTasks = this.state.event.hasOwnProperty('sub_tasks');
        for (let key in this.state.event) {
            let active = (this.itemTypes[key] !== undefined);
            modalValues[key] = {
                onChange: this.props.onChange,
                onCustom: this.props.onCustom,
                type: (active && this.itemTypes[key].type),
                active: active,
                label: (active && this.itemTypes[key].label),
            };
        }
        if (!itemHaveSubTasks) {
            let component = 'sub_tasks';
            this.state.event[component] = [];
            modalValues[component] = {
                onChange: this.props.onChange,
                onCustom: this.props.onCustom,
                type: (this.itemTypes[component].type),
                active: true,
                label: (this.itemTypes[component].label),
                visible: 0                                  // this option set table in hidden state, and view only button create new task (sub_tack)
            };
        }

        this.fields = {};
        this.setState({
            modalShow: true,
            modalContent: this.state.event.name,
            modalData: {
                data: this.state.event,
                values: modalValues
            }
        });
        //console.log(this.state);
    };
    formContext = () => {
        return <h3 style={{textAlign: 'center'}}>{this.props.event.name}</h3>;
    };
    closeModal = () => {
        // console.log(this.fields);
        this.setState({modalShow: false});
        //console.log(typeof this['callBackAfterClose']());
        // if (this.hasOwnProperty('callbackAfterClose')) {
        //     this.callBackAfterClose();
        // }
    };
    onModalSave = (data) => {
        //console.log(data);
        let event = JSON.parse(JSON.stringify(this.state.event));
        if (this.state.type === 2) {
            this.props.operationWithBlock("child", this.state.event, "remove");
            this.props.operationWithBlock("child", this.state.event, "refresh");
        } else if (this.state.type === 3) {
            this.props.operationWithBlock("main", this.state.event, "remove");
            this.props.operationWithBlock("main", this.state.event, "refresh");
        }

        for (let key in this.fields) {
            event[key] = this.fields[key];
            console.log(key + ": " + event[key]);
        }
        let advancedState = {
            modalShow: false
        };

        let fined = this.props.reloadBackground(event);
        if (fined) {
            this.updatePosition(event, false, advancedState);
        }
        //console.log(this.props);

        if (this.props.globalVariables.hasOwnProperty('onModalSave')) {
            this.props.globalVariables['onModalSave'](event);
        }
        // console.log(fined)

    };
    componentDidMount() {
        this.props.eventStates['s'] = this.state;
    }

    componentDidUpdate() {
        // this.props.eventSetStates['f'] = this.setStates;
        this.props.eventStates['s'] = this.state;
    }
    componentWillMount() {

    }
    componentWillUpdate() {

    }
    shouldComponentUpdate(nextProps, nextState) {
        return this.needUpdateComponent(this.state, nextState);
    }
    onMouseDown = (e) => {
        let resize = 0;  //1 - it is resize in left side, 2 - it is resize in right side

        for (let key in e.target.classList) {
            if (e.target.classList[key] === 'resize-right') {
                resize = 2;
            }
            if (e.target.classList[key] === 'resize-left') {
                resize = 1;
            }
        }
        if (e.button === 2) {
            return 0;
        }
        switch (resize) {
            case 1:
                this.setState({
                    resizeLeftDown: true,
                    //zIndex: 1
                });
                break;

            case 2:
                this.setState({
                    resizeRightDown: true,
                    //zIndex: 1
                });
                break;

            default:
                this.setState({
                    down: true,
                    //zIndex: 1
                });
                break;
        }

    };
    onMouseUp = (e) => {
        this.setStates({
            zIndex: 0,
            resizeLeft: false,
            resizeRight: false,
            down: false
        });
    };
    // onMouseMove = (e) => {
    //     if (this.mouseDown
    //         && !this.state.drag
    //         && !this.state.modalShow
    //         && !this.state.resizeRight
    //         && !this.state.resizeLeft) {
    //         // if (e.target.hasClass('resize-right')){
    //             //console.log(e.target.classList);
    //         // }
    //         let resize = 0;  //1 - it is resize in left side, 2 - it is resize in right side
    //
    //         for (let key in e.target.classList) {
    //             if (e.target.classList[key] === 'resize-right') {
    //                 resize = 2;
    //             }
    //             if (e.target.classList[key] === 'resize-left') {
    //                 resize = 1;
    //             }
    //         }
    //         switch (resize){
    //             case 1:
    //                 this.setState({
    //                     resizeLeft: true,
    //                     zIndex: 1
    //                 });
    //                 this.onStartResize(e,resize);
    //                 break;
    //
    //             case 2:
    //                 this.setState({
    //                     resizeRight: true,
    //                     zIndex: 1
    //                 });
    //                 this.onStartResize(e,resize);
    //                 break;
    //
    //             default:
    //                 this.setState({
    //                     drag: true,
    //                     zIndex: 1
    //                 });
    //                 this.onStartDrag(e);
    //                 break;
    //         }
    //
    //     }
    //     if (this.state.drag) {
    //         this.onDrag(e);
    //     } else if (this.state.resizeLeft) {
    //         this.onResizeLeft(e);
    //     } else if (this.state.resizeRight) {
    //         this.onResizeRight(e);
    //     }
    // };
    // onMouseDown = (e) => {
    //     this.mouseDown = true;
    // };
    // onMouseUp = (e) => {
    //     this.mouseDown = false;
    //     if (this.state.drag) {
    //         this.setState({
    //             drag: false,
    //             zIndex: 0
    //         });
    //         this.onEndDrag(e);
    //         this.afterDrag = true;
    //     } else if (this.state.resizeLeft) {
    //         this.setState({
    //             resizeLeft: false,
    //             zIndex: 0
    //         });
    //         this.onEndResize(e,1);
    //     } else if (this.state.resizeRight) {
    //         this.setState({
    //             resizeRight: false,
    //             zIndex: 0
    //         });
    //         this.onEndResize(e,2);
    //     }
    // };
    // onStartResize = (e,side) => {
    //     // console.log(e.target);
    //     this.startWidth = this.width;
    //     this.startX = e.pageX;
    //     this.startLeft = this.state.left;
    //     console.log("start");
    // };
    // onResizeLeft = (e) => {
    //     e.persist();
    //     console.time('resize');
    //     //let event = JSON.parse(JSON.stringify(this.event));
    //     let left = this.startLeft;
    //     let x = e.pageX - this.startX;
    //
    //     this.width = this.startWidth - x;
    //
    //     this.setState({
    //         //event: event
    //         left: left + x
    //         // edit: true
    //     },() => {console.timeEnd('resize')});
    //
    // };
    // onResizeRight = (e) => {
    //     console.log("moveRight");
    // };
    // onEndResize = (e,side) => {
    //     console.log("end");
    // };
    // onStartDrag = (e) => {
    //     //this.props.onStartDrag(this.state.event);
    //     let rect = e.target.getBoundingClientRect();
    //     this.x = e.clientX - rect.left;
    //     this.startX = e.pageX + (this.width/2 - this.x);
    //     this.startY = e.pageY;
    //     this.startTop = this.state.top;
    //     this.startLeft = this.state.left;
    //
    //
    //
    //     this.y = e.clientY - rect.top;
    //     //
    //     // console.log('x: ' + x + "; y: " + y);
    //
    //     if (this.state.type === 2) {
    //         this.props.operationWithBlock("child", this.state.event, "remove");
    //         this.props.operationWithBlock("child", this.state.event, "refresh");
    //     } else if (this.state.type === 3) {
    //         this.props.operationWithBlock("main", this.state.event, "remove");
    //         this.props.operationWithBlock("main", this.state.event, "refresh");
    //     }
    // };
    // onEndDrag = (e) => {
    //     this.props.onEndDrag(this.state.event);
    //     let top = 0;
    //     let left = 0;
    //     if (this.state.edit) {
    //         top = this.state.top;
    //         left = this.state.left;
    //     } else {
    //         top = this.startTop;
    //         left = this.startLeft;
    //     }
    //     let type = 0;
    //     let item = this.props.resources[0];
    //     let state = {};
    //
    //     for (let key in this.props.resources) {
    //         let item_resource = this.props.resources[key];
    //         if (top < item_resource.top) {
    //             break;
    //         } else {
    //             item = this.props.resources[key];
    //         }
    //     }
    //     //console.log(item);
    //     let event = JSON.parse(JSON.stringify(this.state.event));
    //     if (item.type === 'main') {
    //         type = 3;
    //         top = item.top;
    //         event.resource = item.element.id;
    //         event.responsible_uid = "0";
    //         event.end = "0";
    //         this.props.operationWithBlock("main", event, "add");
    //         this.props.operationWithBlock("main", event, "refresh");
    //     } else {
    //         if (top < (item.top + config.height_cell)) {
    //             type = 1;
    //             top = item.top;
    //             let head_item = this.props.header[0];
    //             for (let key in this.props.header) {
    //                 if (left < this.props.header[key].values.left) {
    //                     break;
    //                 } else {
    //                     head_item = this.props.header[key];
    //                 }
    //             }
    //             event.responsible_uid = item.element.id;
    //             event.start = format(head_item.values.date, 2);
    //
    //             //if (!data_class.isDate(event.end)) {
    //             let d = head_item.values.date;
    //             d.setDate(head_item.values.date.getDate() + 1);
    //             event.end = format(d, 2);
    //             //}
    //             state['left'] = head_item.values.left;
    //         } else {
    //             top = item.top + config.height_cell + 1;
    //             type = 2;
    //             event.responsible_uid = item.element.id;
    //
    //             event.end = "0";
    //             this.props.operationWithBlock("child", event, "add");
    //             this.props.operationWithBlock("child", event, "refresh");
    //         }
    //     }
    //     state['top'] = top;
    //     state['type'] = type;
    //     state['event'] = event;
    //     this.setState(state);
    //     //console.log("top " + top + " left " + left);
    // };
    // onDrag = (e) => {
    //     e.persist();
    //     let top = this.startTop;
    //     let left = this.startLeft;
    //     let y = e.pageY - this.startY;
    //     let x = e.pageX - this.startX;
    //     this.setState({
    //         top: (y + top),
    //         left: (x + left),
    //         // edit: true
    //     });
    // };
    render() {
        // console.log(this.state.event);
        //console.log(this.state.width);
        //console.log(this.Recived);
        let style = {
            position: 'absolute',
            overflow: 'hidden',
            textAlign: "center",
            cursor: 'pointer',
            userSelect: "none",
            borderRadius: "4px"
        };

        if (this.state.zIndex > 0) {
            style['zIndex'] = this.state.zIndex;
        }
        let class_name = "";
        let resizeBlocks = [];
        switch (this.state.type) {
            case 1:

                this.width = this.state.width;
                style['backgroundColor'] = config.event_color;
                style['height'] = config.height_cell + "px";
                if (this.state.resizeRight) {
                    style['width'] = (this.width + this.state.resizeRightNum) + "px";
                } else if (this.state.resizeLeft) {
                    style['width'] = (this.width - this.state.resizeLeftNum) + "px";
                } else {
                    let width = 0;
                    if (this.state.width === 0) {
                        let widthEvent = getSubtract(new Date(this.state.event.start), new Date(this.state.event.end));
                        // console.log(widthEvent);
                        width = config.width_cell * (widthEvent+1) + widthEvent;
                    } else {
                        width = this.state.width;
                    }
                    style['width'] = width + "px";
                    // console.log(width);
                }
                // style['width'] = this.width + "px";
                style['top'] = this.state.top + "px";
                if (this.state.resizeLeft) {
                    style['left'] = (this.state.left + this.state.resizeLeftNum) + "px";
                } else {
                    style['left'] = this.state.left + "px";//(left * config.width_cell + left) + "px";
                }

                class_name = "active-event-block";
                resizeBlocks.push(<div
                    key="left"
                    className="resize-left"
                    style={{
                        width: "5px",
                        height: config.height_cell + "px",
                        cursor: 'w-resize',
                        backgroundColor: "red",
                        position: 'absolute',
                        opacity: 0,
                        left: "1px"
                    }}
                    event_id={this.state.event.id}
                />);
                resizeBlocks.push(<div
                    key="right"
                    className="resize-right"
                    style={{
                        width: "5px",
                        height: config.height_cell + "px",
                        cursor: 'e-resize',
                        backgroundColor: "red",
                        position: 'absolute',
                        opacity: 0,
                        right: "1px"
                    }}
                    event_id={this.state.event.id}
                />);
                break;
            case 2: //don't have a date
                this.width = config.width_cell;
                style['backgroundColor'] = "#eee";
                style['height'] = config.height_cell + "px";
                style['width'] = this.width + "px";
                style['top'] = (this.state.top) + "px";
                style['left'] = this.state.left + "px";//(left*config.width_cell+left) + "px";
                break;
            default: //don't have responsible (and date)
                this.width = config.width_cell;
                style['backgroundColor'] = "#eee";
                style['width'] = this.width + "px";
                style['height'] = (config.height_cell * 2 + 1) + "px";
                style['top'] = this.state.top + "px";
                style['left'] = this.state.left + "px";//(left*config.width_cell+left) + "px";
                break;
        }
        //this.fields = {};
        if (this.state.noDrop) {
            //class_name += class_name === "" ? 'no-drop' : ' no-drop';
            style['cursor'] = 'no-drop'
        }
        //console.log(this.state);
        // console.log(JSON.parse(JSON.stringify(this.state.event)));
        // }
        //console.log(this.state);
        return <div
            event_id={this.state.event.id}
            className={class_name}
            style={style}
            onClick={() => {
                this.onClick(this.state.event)
            }}
            onDoubleClick={() => {
                this.onDobleClick()
            }}
            onMouseDown={(e) => this.onMouseDown(e)}
            // onMouseMove={(e) => this.onMouseMove(e)}
            onMouseUp={(e) => this.onMouseUp(e)}
        >
            {resizeBlocks}
            {this.state.event.name}
            {this.state.modalShow && (
                <TModalWindow
                    close={() => this.closeModal()}
                    sub_res={this.props.sub_res}
                    data={this.state.modalData.data}
                    action={(data) => this.onModalSave(data)}
                    values={this.state.modalData.values}
                    fields={this.fields}
                    type={2} >
                    {this.state.modalContent}
                </TModalWindow>
            )}
        </div>;
    }

}

export default EventBlock;