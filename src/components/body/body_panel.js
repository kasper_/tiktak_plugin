import React from 'react';
import ItemBody from "./item_body";
import {config} from "../../core/config";
import ShadowBlock from "./shadow_block";


class BodyPanel extends React.Component {


    render () {
        let html = [];
        let id = 0;
        // console.log(this.props.header,this.props.left);
        //console.log(JSON.stringify(this.props.left));
        // let temp = 0;
        for (let key in this.props.header) {
            let header = this.props.header[key];
            //console.log(header);
            for (let key1 in this.props.left) {
                let left = this.props.left[key1];
                //console.log(left);

                if (left.type === 'main') {
                    id = left.element.id;
                    continue;
                }
                let k1 = header.values.left + "_" + left.top;
                let k2 = header.values.left + "_" + left.top + "_1";
                //console.log(k1,k2);
                // if (id === "8") {
                //     console.log(left);
                // }
                // html.push(<ItemBody key={k1} style={{
                //     left: header.values.left + "px",
                //     top: left.top + "px"
                // }}
                // id={id}
                //
                // >{k1}</ItemBody>);
                // html.push(<ItemBody key={k2} style={{
                //     left: header.values.left + "px",
                //     top: (left.top + 1 + config.height_cell) + "px"
                // }}
                // id={id}
                //
                // >{k2}</ItemBody>);
                html.push(<ItemBody key={k1} style={{
                    left: header.values.left + "px",
                    top: left.top + "px"
                }}
                                    id={id}

                />);
                html.push(<ItemBody key={k2} style={{
                    left: header.values.left + "px",
                    top: (left.top + 1 + config.height_cell) + "px"
                }}
                                    id={id}

                />);
                // temp ++;
            }
            // console.log(temp);
        }

        let importFunctions = this.props.shadowBlockStates;

        return <div className={"body-panel"} style={{
            position: 'absolute',
            left: config.width_left_panel + "px",
            top: (config.height_cell + 2) + 'px',
        }}>{html}{<ShadowBlock height={this.props.heightBody} width={this.props.widthBody} importFunctions={importFunctions}/>}</div>
    }
}

export default BodyPanel;