import React from 'react'
import {config} from "../../../core/config";


class ItemBody extends React.Component {

    setStyle = (style) => {
        for (let key in style) {
            this.style[key] = style[key];
        }
    };

    componentWillUpdate() {
        this.style = {
            width: config.width_cell + "px",
            height: config.height_cell + "px",
            position: 'absolute',
            backgroundColor: '#fff'
        };
        if (typeof this.props.style === 'object'){
            this.setStyle(this.props.style);
        }
        // if (this.props.id === "8") {
        //     console.log(this.props.style);
        // }
        //console.log('update');
    }

    componentWillMount() {
        this.style = {
            width: config.width_cell + "px",
            height: config.height_cell + "px",
            position: 'absolute',
            backgroundColor: '#fff'
        };
        if (typeof this.props.style === 'object'){
            this.setStyle(this.props.style);
        }
        // if (this.props.id === "8") {
        //     console.log(this.props.style);
        // }
    }
    render () {
        //console.log(this.props.left);
        return <div style={this.style}>{this.props.children}</div>
    }
}

export default ItemBody;