import React from 'react';
import {config} from "../../core/config";
import ProjectBlock from "./project_block";
import EventBlock from "./event_block";
// import {format} from "../../functions/dateHelper";


class EventsBody extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            selected: null,
            drag: false,
            resizeLeft: false,
            resizeRight: false
        };
        this.mouseDown = false;
        this.event = null;
        this.eventsStates = {};
    }

    onMouseUp = (e) => {

        if (this.state.drag) {
            this.onEndDrag(e, this.state.event);
            this.mouseDown = false;
            this.setState({
                event: null,
                drag: false
            });

        }
    };

    onMouseMove = (e) => {
        if (!this.mouseDown) {
            return 0;
        }
        this.event = this.props.events[e.target.getAttribute('event_id')];
        this.eventsSetStates[this.event.id].f({
            drag: true,
            zIndex: 1
        });

        if (!this.state.drag) {
            this.setState({
                drag: true
            });
            this.onStartDrag(e, this.event);
            this.onDrag(e, this.event);
        } else {
            this.onDrag(e, this.event);
        }

    };

    onStartDrag = (e, event, multiple = false) => {
        let rect = e.target.getBoundingClientRect();
        this.x = e.clientX - rect.left;
        this.startX = e.pageX;
        this.startY = e.pageY;
        this.startTop = this.eventsStates[event.id].s.top;
        this.startLeft = this.eventsStates[event.id].s.left;

        if (this.eventsStates[event.id].s.type === 2) {
            this.operationWithBlock("child", event, "remove");
            this.operationWithBlock("child", event, "refresh");

        } else if (this.eventsStates[event.id].s.type === 3) {
            this.operationWithBlock("main", event, "remove");
            this.operationWithBlock("main", event, "refresh");

        }
    };

    onDrag = (e, event, multiple = false) => {
        let top = this.startTop;
        let left = this.startLeft;
        let y = e.pageY - this.startY;
        let x = e.pageX - this.startX;
        this.eventsSetStates[event.id].f({
            top: (y + top),
            left: (x + left)
        });
    };

    onEndDrag = (e, event, multiple = false) => {

    };

    componentDidUpdate() {
        this.props.importFunctions['operationWithBlock'] = this.operationWithBlock;
        this.props.importFunctions['viewAllEventsInBlock'] = this.viewAllEventsInBlock;
    }

    componentDidMount() {
        this.props.importFunctions['operationWithBlock'] = this.operationWithBlock;
        this.props.importFunctions['viewAllEventsInBlock'] = this.viewAllEventsInBlock;
    }

    selectEvent = (event) => {

    };

    // this method called to setState event-block - set in first params

    updateEventBlock = (event,stateData) => {
        this.eventsStates[event.id].f(stateData)
        //console.log(event);
    };

    viewAllEventsInBlock = (type, element, resource) => {
        if (type === 'child') {
            return this.childBlocksPosition[resource.id + "_" + element.id];
        } else {
            return this.mainBlocksPosition[element.id];
        }
    };

    operationWithBlock = (type,event,typeOperation) => {
        switch (typeOperation) {
            case "remove":
                if (type === 'child') {
                    //console.log(JSON.parse(JSON.stringify(this.childBlocksPosition[event.resource + "_" + event.responsible_uid].events)));
                    delete this.childBlocksPosition[event.resource + "_" + event.responsible_uid].events[event.id];
                    //console.log(JSON.parse(JSON.stringify(this.childBlocksPosition[event.resource + "_" + event.responsible_uid].events)));
                } else if (type === 'main') {
                    //console.log(this.mainBlocksPosition[event.resource].events[event.id]);
                    delete this.mainBlocksPosition[event.resource].events[event.id];
                }
                //console.log('remove');
                break;
            case "add":
                if (type === 'child') {
                    if (!this.childBlocksPosition.hasOwnProperty(event.resource + "_" + event.responsible_uid)) {
                        this.childBlocksPosition[event.resource + "_" + event.responsible_uid] = {
                            events: {},
                            left: 0
                        }
                    }
                    this.childBlocksPosition[event.resource + "_" + event.responsible_uid].events[event.id] = event;
                } else if (type === 'main') {
                    this.mainBlocksPosition[event.resource].events[event.id] = event;
                }
                break;
            case "refresh":

                if (type === 'child') {
                    let pos = 0;
                    // console.log(this.childBlocksPosition[event.resource + "_" + event.responsible_uid]);
                    for (let key in this.childBlocksPosition[event.resource + "_" + event.responsible_uid].events) {
                        let event_ = this.childBlocksPosition[event.resource + "_" + event.responsible_uid].events[key];
                        //console.log(event_);
                        this.updateEventBlock(event_, {position: pos});
                        pos++;
                    }
                    // console.log(this.childBlocksPosition[event.resource + "_" + event.responsible_uid].events);
                    this.childBlocksPosition[event.resource + "_" + event.responsible_uid].left = pos;
                } else if (type === 'main') {
                    let pos = 0;
                    for (let key in this.mainBlocksPosition[event.resource].events) {
                        let event_ = this.mainBlocksPosition[event.resource].events[key];
                        //console.log(event_);
                        this.updateEventBlock(event_, {position: pos});
                        pos++;
                    }
                    this.mainBlocksPosition[event.resource].left = pos;
                }
                //console.log('refresh');
                break;
            default:
                if (type === 'child') {
                    let pos = 0;
                    for (let key in this.childBlocksPosition[event.resource + "_" + event.responsible_uid].events) {
                        let event_ = this.childBlocksPosition[event.resource + "_" + event.responsible_uid].events[key];
                        //console.log(event_);
                        this.updateEventBlock(event_, {position: pos});
                        pos++;
                    }
                    this.childBlocksPosition[event.resource + "_" + event.responsible_uid].left = pos;
                } else if (type === 'main') {
                    let pos = 0;
                    for (let key in this.mainBlocksPosition[event.resource].events) {
                        let event_ = this.mainBlocksPosition[event.resource].events[key];
                        //console.log(event_);
                        this.updateEventBlock(event_, {position: pos});
                        pos++;
                    }
                    this.mainBlocksPosition[event.resource].left = pos;
                }
                break;
        }
    };

    componentWillMount() {

    }

    onChange = (event, key, value) => {
        //console.log(event, key, value);
    };

    onCustom = (event, params) => {
        //console.log(event, params);
    };

    render () {
        //let resources = this.props.resources;

        //console.log(JSON.stringify(this.props.left));
        let left = this.props.left;
        let points = Object.keys(this.props.header).length;

        this.html = [];
        this.mainBlocksPosition = {};
        this.childBlocksPosition = {};
        let activePropject = null;
        for (let key in left) {
            let res = left[key];
            if (res.type === 'main') {
                activePropject = res.element;
                this.mainBlocksPosition[res.element.id] = {
                    top: res.top,
                    left: 0,
                    events: {}
                };
                this.html.push(<ProjectBlock
                    key={res.element.id}
                    style={{
                        top: res.top + "px",
                        width: (points*config.width_cell+points) + "px",
                        backgroundColor: res.element.color
                    }}/>)
            } else {
                this.childBlocksPosition[activePropject.id + "_" + res.element.id] = {
                    top: res.top,
                    left: 0,
                    events: {}
                };
            }
            //console.log(this.childBlocksPosition);
        }
        for (let key in this.props.events) {
            let event = this.props.events[key];
            if (!this.eventsStates.hasOwnProperty(event.id)){
                this.eventsStates[event.id] = {};
            }

            this.html.push(<EventBlock
                key={"e_" + event.id}
                event={event}
                onChange={this.onChange}
                onCustom={this.onCustom}
                select={this.selectEvent}
                mainBlocks={this.mainBlocksPosition}
                childBlocks={this.childBlocksPosition}
                sub_res={this.props.sub_res}
                resources={this.props.left}
                header={this.props.header}
                operationWithBlock={this.operationWithBlock}
                // eventSetStates={this.eventsSetStates[event.id]}  //this variable pick function setStates from eventblock
                reloadBackground={this.props.reloadBackground}
                eventStates={this.eventsStates[event.id]}
                dragEvents={this.props.dragEvents}
                importFunctions={this.props.importFunctions}
                globalVariables={this.props.globalVariables}
            />);
            this.props.eventsStates[event.id] = this.eventsStates[event.id];
        }

        return <div
            className={"body-panel"}
            style={{
                position: 'absolute',
                left: config.width_left_panel + "px",
                top: (config.height_cell + 2) + 'px',
                height: this.props.height + "px",
                width: this.props.width + "px",
                overflow: 'hidden',
            }}
            // onMouseDown={(e) => this.onMouseDown(e)}
            // // onMouseMove={(e) => this.onMouseMove(e)}
            // onMouseUp={(e) => this.onMouseUp(e)}
            // onMouseLeave={(e) => this.onMouseOut(e)}
        >{this.html}</div>;
    }
}

export default EventsBody;