import React from 'react'
import ReactSelect from "react-select"


class FormSelect extends React.Component{

    state = null;

    onChange = (data) => {
        this.setState(data);
        this.props.onChange(this.props.params.ref,data.value);
        this.props.params.onChange(this.props.event,this.props.params.ref,data.value);
        //console.log(data);
    };

    render () {

        let options = [];

        for (let key in this.props.list){
            let resp = this.props.list[key];

            options.push({
                label: resp.name,
                value: resp.id
            });
        }
        const selectedOption = this.state === null ? (+this.props.params.defaultValue) : this.state.value;
        const selectedOptionObj = this.state === null ? selectedOption : this.state;
        var element = this.props.list[selectedOption];
        let userName = element !== undefined ? <span style={{fontWeight: 'bold'}}>({element.name})</span> : "";
        let select = <ReactSelect ref={this.props.params.ref}
            value={selectedOptionObj}
            onChange={this.onChange}
            options={options}
        />;
        return <div><label style={{textAlign: "left"}}>{this.props.params.label} {userName}</label>{select}</div>
    }
}

export default FormSelect;