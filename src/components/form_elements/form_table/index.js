import React from 'react'
import './style.css'
import CreateItem from "../form_create_item";

class FormTable extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            visibleTable: props.visible !== 0
        }
    }

    componentWillReceiveProps(props) {
        let state = {
            visibleTable: props.visible !== 0
        };
        this.setState(state);
    }

    fieldsArray = [
        {
            name: "name",
            label: "Text",
            behavior: {
                type: "openForm"
            }
        },
        {
            name: "checked",
            label: "Action",
            behavior: {
                type: "link",
                label: (checked) => {
                    return checked === 0 ? "In work" : "Checked";
                }
            }
        }
    ];

    render () {
        let html = [];
        let htmlHead = [];
        let htmlCreateNew = <CreateItem style={{
            right: "5px",
            position: "absolute"
        }}/>;
        if (this.state.visibleTable) {

            let data = this.props.value;
            let params = this.props.params;
            let row = [];
            for (let j = 0; j < this.fieldsArray.length; j++){
                let field = this.fieldsArray[j];
                row.push(<td key={j}>{field.label}</td>);
            }
            htmlHead.push(<thead key={1}><tr>{row}</tr></thead>);
            for (let i = 0; i < data.length; i++){
                let row = [];
                let event = data[i];
                for (let j = 0; j < this.fieldsArray.length; j++){
                    let field = this.fieldsArray[j];
                    let element = event[field.name];
                    if (field.behavior !== undefined && field.behavior.hasOwnProperty("type")){
                        switch (field.behavior.type){
                            case 'link':
                                row.push(<td key={j} className="td-decoration-element"><span onDoubleClick={() => {params.onCustom(event,field); }} >{field.behavior.label(element)}</span></td>);
                                break;
                            case 'openForm':
                                row.push(<td key={j} style={{cursor: "pointer"}} onDoubleClick={() => params.onCustom(event,field)}>{element}</td>);
                                break;
                            default :
                                row.push(<td key={j}>{element}</td>);
                                break;
                        }
                    } else {
                        row.push(<td key={j}>{element}</td>);
                    }
                }
                html.push(<tr key={event.id}>{row}</tr>);
            }
            return <div style={{
                overflowY: "scroll",
                maxHeight: "200px"
            }}><label style={{textAlign: "left"}}>{this.props.params.label}</label>{htmlCreateNew}<table className="simple-little-table">{htmlHead}<tbody>{html}</tbody></table></div>;

        } else {
            return <div>{htmlCreateNew}</div>;
        }
    };

}

export default FormTable;