import React from 'react'
import  * as Datetime from 'react-datetime/DateTime'
import 'react-datetime/css/react-datetime.css'
import {format} from '../../functions/dateHelper'

class DateTime extends React.Component{

    constructor(props){
        super(props);
        //this.onChange = this.onChange.bind(this);
        // this.state = {
        //     'defaultValue': this.props.value
        // }
    }

    onChange = (moment) => {
        this.props.onChange(this.props.params.ref,format(moment._d,2));
        this.props.params.onChange(this.props.event,this.props.params.ref,format(moment._d,2));
        // console.log(format(moment._d,2));
    };

    render () {
        let input = <Datetime
            onChange={this.onChange}
            defaultValue={this.props.value}
            dateFormat={"YYYY-MM-DD"}
            timeFormat={"HH:mm:ss"}/>;
        return <div key={this.props.name} ><label style={{textAlign: "left"}}>{this.props.params.label}</label>{input}</div>;
    }

}

export default DateTime;
