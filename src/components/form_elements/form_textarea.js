import React from 'react'


class FormTextArea extends React.Component{

    constructor(props){
        super(props);
        this.onChange = this.onChange.bind(this);
    }

    onChange = () => {
        this.props.onChange(this.props.params.ref,this.refs[this.props.params.ref].value);
        this.props.params.onChange(this.props.event, this.props.params.ref,this.refs[this.props.params.ref].value);
    };

    render (){
        let input = React.createElement("textarea", {
            style: {
                width: "280px",
                height: "60px"
            },
            onChange: this.onChange,
            ref: this.props.params.ref,
            defaultValue: this.props.value});
        return <div key={this.props.name} ><label style={{textAlign: "left"}}>{this.props.params.label}</label><br/>{input}</div>
    }

}

export default FormTextArea;