import React from 'react'
import './style.css'
import CreateItem from "../form_create_item";
// import TModalWindow from "../../tt_modal_window";

class FormCustomTable extends React.Component{

    constructor (props) {
        super(props);

        this.state = {
            visibleTable: props.visible !== 0
        };
        // let eventFormsStates = {};
        // for (let key in props.value) {
        //     eventFormsStates[key] = false;
        // }
        // this.state = {
        //     formsStates: eventFormsStates
        // }

        this.fieldsArray = [
            {
                name: "number",
                label: "#",
                behavior: {
                    type: "label"
                }
            },
            {
                name: "events",
                label: "Events",
                behavior: {
                    type: "openForm",
                    label: (data) => {
                        return data;
                    }
                }
            }
        ];

    }

    componentWillReceiveProps(props) {
        let state = {
            visibleTable: props.visible !== 0
        };
        this.setState(state);
    }



    render () {
        let html = [];
        let htmlHead = [];

        let data = this.props.value;
        let params = this.props.params;
        // console.log(params);
        let htmlCreateButton = <CreateItem style={{
            right: "5px",
            position: "absolute"
        }}/>;
        // return "";
        // console.log(data);
        let row = [];
        for (let j = 0; j < this.fieldsArray.length; j++){
            let field = this.fieldsArray[j];
            row.push(<td key={j}>{field.label}</td>);
        }
        htmlHead.push(<thead key={1}><tr style={{
            fontWeight: 'bolder'
        }}>{row}</tr></thead>);

        if (this.state.visibleTable) {
            let numerator = 1;
            for (let key in data) {
                row = [];
                let event = data[key];
                for (let j = 0; j < this.fieldsArray.length; j++){
                    let field = this.fieldsArray[j];
                    switch (field.name) {
                        case 'number':
                            row.push(<td
                                key={j}
                                style={{
                                    textAlign: "center"
                                }}>{numerator}</td>);
                            break;
                        case 'events':
                            row.push(<td
                                key={j}
                                onDoubleClick={(e) => params.onChange(e,event.id)}
                                style={{
                                    textAlign: "center"
                                }}>{event.name}
                            </td>);
                            break;
                    }
                }
                html.push(<tr key={event.id}>{row}</tr>);
                numerator++;
            }

            return <div style={{
                overflowY: "scroll",
                maxHeight: "200px"
            }}><label
                style={{textAlign: "left"}}>
                {this.props.params.label}
                </label>{htmlCreateButton}
                <table className="simple-little-table">
                    {htmlHead}
                    <tbody>
                    {html}
                    </tbody>
                </table>
            </div>;
        } else {
            return <div style={{
                overflowY: "scroll",
                maxHeight: "200px"
            }}>
                <label
                    style={{textAlign: "left"}}>
                    {this.props.params.label}
                </label>{htmlCreateButton}
                <table className="simple-little-table">
                    {htmlHead}
                </table>
            </div>
        }

    };

}

export default FormCustomTable;