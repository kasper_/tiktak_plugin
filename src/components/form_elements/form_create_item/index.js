import React from 'react';
import './style.css';
import Image from './download.png';

class CreateItem extends React.Component {

    constructor(props) {
        super(props);
        this.style = {
            position: "relative"
        };
    }

    onClick = () => {

    };

    setStyles = (styles) => {
        if (typeof styles === "object") {
            for (let key in styles) {
                this.style[key] = styles[key];
            }
        }
    };

    render () {
        if (this.props.style !== undefined) {
            this.setStyles(this.props.style);
        }

        let styleImage = {
            width: "25px",
            height: "25px"
        };

        return <div style={{
            position: 'relative',
            height: '30px'
        }}><button
            onClick={this.onClick}
            style={this.style} >
            <img
                style={styleImage}
                src={Image}
            />
        </button>
        </div>;
    }
}

export default CreateItem;