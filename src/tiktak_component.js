import React from 'react';
import {format, getSubtract} from './functions/dateHelper'
import {config} from "./core/config"
import TEvents from './components/tt_events'
import TResource from './components/tt_resource'

class TiktakComponent extends React.Component {

    state = {
        st: 1
    };

    values = {};

    matrix = {};

    constructor() {
        super();
        this.widthMatrix = 0;
        this.heightMatrix = 0;
        this.widthHeader = 0;
        this.heightHeader = 0;
        this.type = 1;
    }

    static setResources(resources) {
        this.resources = {};
        for (let key in resources){
            let res = resources[key];
            this.resources[res.id] = res;
        }
        //resources.map((resource) => {this.resources[resource.id] = resource; });
        //console.log(this.resources);
    }

    static now;
    static tomorrow;
    static allResource;
    static minDate;
    static maxDate;
    static onePointInMs;
    static pointRes;

    static setEvents(events) {
        this.events = {};
        for (let key in events) {
            let obj = events[key];
            this.events[obj.id] = obj;
        }

    }
    static setResponsibles(responsibles) {
        this.responsibles = {};
        for (let key in responsibles) {
            let responsible = responsibles[key];
            this.responsibles[responsible.id] = responsible;
        }
    }

    static updateDatesArray(timeline_point) {
        this.datesArray = [];
        for (let i = 0; i < this.pointsLine; i++) {
            let val = new Date(this.minDate.getTime() + this.onePointInMS * i);
            switch (timeline_point) {
                case 'day':
                    val = {
                        val: val.getDate(),
                        time: val
                    };
                    break;
                case 'hour':
                    val = {
                        val: val.getHours(),
                        time: val
                    };
                    break;
                case 'minute':
                    val = {
                        val: val.getMinutes(),
                        time: val
                    };
                    break;
                default :
                    val = {
                        val: val.getDate(),
                        time: val
                    };
            }
            this.datesArray.splice(i, 0, val);
        }
    }

    static returnPointInMs(timeline_point) {
        let result = 1000 * 60 * 60 * 24;
        switch (timeline_point) {
            case 'day':
                result = 1000 * 60 * 60 * 24;
                break;
            case 'hour':
                result = 1000 * 60 * 60;
                break;
            case 'minute':
                result = 1000 * 60;
                break;
            default :
                break;
        }
        return result;
    }

    static isDate(strDate) {
        let result = false;
        if (strDate == null) {
            return result;
        }
        if (strDate === "") {
            return result;
        }
        if (strDate === "0") {
            return result;
        }
        if (strDate === 0) {
            return result;
        }
        return true;
    }

    static update() {
        let now = new Date();
        this.onePointInMS = this.returnPointInMs(this.initParams.timeline_point);
        let tomorrow = new Date(now.getTime() + this.onePointInMS);
        let minDate = new Date().getTime();
        let maxDate = new Date("2000.01.01").getTime();
        TiktakComponent.allResource = {};
        for (let key in this.events) {
            let obj = this.events[key];
            let start = this.isDate(obj.start);
            if (!start) {
                start = this.initParams.dev;
                this.events[key]['start'] = (this.initParams.dev ? format(now) : this.events[key]['start']);
            }
            let end = this.isDate(obj.end);
            if (!end) {
                end = this.initParams.dev;
                this.events[key]['end'] = (this.initParams.dev ? format(tomorrow) : this.events[key]['end']);
            }
            //let ids = obj.resource.split("_");
            obj['full_parent'] = obj.resource + "_" + obj.responsible_uid;
            if (!isNaN(obj.responsible_uid) && obj.responsible_uid > 0) {
                if (!start || !end) {
                    this.events[key]['duration'] = 0;
                    this.events[key]['start'] = start ? obj.start : "0";
                    this.events[key]['end'] = end ? obj.end : "0";
                } else {
                    let startday = new Date(obj.start);
                    let endday = new Date(obj.end);
                    this.events[key]['duration'] = getSubtract(startday, endday);
                    let mind = startday.getTime();
                    minDate = mind < minDate ? mind : minDate;
                    let maxd = endday.getTime();
                    maxDate = maxd > maxDate ? maxd : maxDate;

                    if (!TiktakComponent.allResource.hasOwnProperty("r_" + obj.resource)) {
                        TiktakComponent.allResource["r_" + obj.resource] = {
                            minDate: mind,
                            maxDate: maxd
                        };
                    } else {
                        if (TiktakComponent.allResource["r_" + obj.resource]['minDate'] > mind) {
                            TiktakComponent.allResource["r_" + obj.resource]['minDate'] = mind;
                        }
                        if (TiktakComponent.allResource["r_" + obj.resource]['maxDate'] < maxd) {
                            TiktakComponent.allResource["r_" + obj.resource]['maxDate'] = maxd;
                        }
                    }
                }

            } else {
                obj['full_parent'] = obj.resource;
            }
        }
        this.minDate = new Date(minDate - this.onePointInMS);
        this.maxDate = new Date(maxDate + this.onePointInMS);
        TiktakComponent.minDate = this.minDate;
        TiktakComponent.maxDate = this.maxDate;
        this.pointsLine = getSubtract(this.minDate, this.maxDate);
        this.updateDatesArray(this.initParams.timeline_point);
        //console.log(this.datesArray);
        this.makeArray();
    }

    static init(min = "", max = "", timeline_point = "day", type = 1, dev = true) {

        this.initParams = {
            timeline_point: timeline_point,
            type: type,
            dev: dev
        };

        let now = new Date();
        this.now = now;
        this.onePointInMS = this.returnPointInMs(timeline_point);
        let tomorrow = new Date(now.getTime() + this.onePointInMS);
        this.tomorrow = tomorrow;
        let minDate = min === "" ? new Date().getTime() : new Date(min).getTime();
        let maxDate = max === "" ? new Date("2000.01.01").getTime() : new Date(max).getTime();
        TiktakComponent.allResource = {};
        for (let key in this.events) {
            let obj = this.events[key];
            let start = this.isDate(obj.start);
            if (!start) {
                start = dev;
                this.events[key]['start'] = (dev ? format(now) : this.events[key]['start']);
            }
            let end = this.isDate(obj.end);
            if (!end) {
                end = dev;
                this.events[key]['end'] = (dev ? format(tomorrow) : this.events[key]['end']);
            }

            obj['full_parent'] = obj.resource + "_" + obj.responsible_uid;
            if (!isNaN(obj.responsible_uid) && obj.responsible_uid > 0) {
                    if (!start || !end) {
                        this.events[key]['duration'] = 0;
                        this.events[key]['start'] = start ? obj.start : "0001-01-01";
                        this.events[key]['end'] = end ? obj.end : "0001-01-01";
                    } else {
                        let startday = new Date(obj.start);
                        let endday = new Date(obj.end);
                        this.events[key]['duration'] = getSubtract(startday, endday);
                        let mind = startday.getTime();
                        minDate = mind < minDate ? mind : minDate;
                        let maxd = endday.getTime();
                        maxDate = maxd > maxDate ? maxd : maxDate;

                    //     if (!TiktakComponent.allResource.hasOwnProperty("r_" + obj.resource)) {
                    //         TiktakComponent.allResource["r_" + obj.resource] = {
                    //             minDate: mind,
                    //             maxDate: maxd
                    //         };
                    //
                    //     } else {
                    //         if (TiktakComponent.allResource["r_" + obj.resource]['minDate'] > mind) {
                    //             TiktakComponent.allResource["r_" + obj.resource]['minDate'] = mind;
                    //         }
                    //         if (TiktakComponent.allResource["r_" + obj.resource]['maxDate'] < maxd) {
                    //             TiktakComponent.allResource["r_" + obj.resource]['maxDate'] = maxd;
                    //         }
                    //     }
                    }

            } else {
                obj['full_parent'] = obj.resource;
            }
        }


        this.minDate = new Date(minDate - this.onePointInMS);
        this.maxDate = new Date(maxDate + this.onePointInMS);
        TiktakComponent.minDate = this.minDate;
        TiktakComponent.maxDate = this.maxDate;
        for (let key in this.resources){
            let res = this.resources[key];
            TiktakComponent.allResource["r_" + res.id] = {
                minDate: this.minDate,
                maxDate: this.maxDate
            };

        }
        this.pointsLine = getSubtract(this.minDate, this.maxDate);
        this.updateDatesArray(timeline_point);
        //console.log(this.datesArray);
        this.makeArray();
    }

    static initParams = {

    };

    static makeArray() {

    }

    static cellStyle(left, top, color = "#fff", width = 0, height = 0) {
        return {
            position: 'absolute',
            width: width === 0 ? config.width_cell : width + "px",
            height: height === 0 ? config.height_cell : height + "px",
            left: left + "px",
            top: top + "px",
            backgroundColor: color,
        };
    }

    modify = () => {
        if (Object.keys(this.matrix).length !== 0){
            TiktakComponent.update();
        }
        this.setState({st: 1});
        console.log("modify");
    };

    render() {
        console.log("render");
        let html = [];
        let htmlHead = [];
        let htmlLeft = [];
        let htmlEvents = [];
        let htmlResources = [];
        TiktakComponent.pointRes = [];
        this.matrix = {};
        let iteracion = 0;
        for (let i = 0; i < TiktakComponent.datesArray.length; i++) {

            let left = i + config.width_cell * i;
            let top = 1;
            htmlHead.push(<div key={"head_" + i} style={{
                position: 'absolute',
                width: config.width_cell + "px",
                height: config.height_cell + "px",
                left: left + "px",
                top: top + "px",
                backgroundColor: "#fff",
                textAlign: "center"
            }}>{TiktakComponent.datesArray[i].val}</div>);
            this.widthHeader = left;
        }
        this.heightHeader = config.height_cell + 1;
        this.widthHeader += config.width_cell + 1;
        htmlLeft.push(<div key={"left_head"} style={{
            height: "" + (this.heightHeader - 1) + "px",
            width: "274px",
            backgroundColor: "#fff",
            position: "absolute",
            left: "1px",
            top: "1px"
        }}/>);

        //console.log(this);
        for (let key in TiktakComponent.resources) {
            let obj = TiktakComponent.resources[key];
            //console.log(TiktakComponent.datesArray);
            for (let i = 0; i < TiktakComponent.datesArray.length; i++){
                let num = 0;
                let left = i+config.width_cell*i;
                let top = (2*iteracion+1+config.height_cell*iteracion)+(((iteracion+num)*config.height_cell)+num);
                let style = TiktakComponent.cellStyle(left,top);
                html.push(<div key={"l_" + i + "_t_" + iteracion + "_n_" + num} style={style}/>);
                if (!this.matrix.hasOwnProperty("r_" + obj.id)){
                    this.matrix["r_" + obj.id] = {
                        top: top
                    };
                }
                //alert(this.matrix);
                num++;
                left = i+config.width_cell*i;
                top = (2*iteracion+1+config.height_cell*iteracion)+(((iteracion+num)*config.height_cell)+num);
                style = TiktakComponent.cellStyle(left,top);
                html.push(<div key={"l_" + i + "_t_" + iteracion + "_n_" + num} style={style}/>);
                if (!this.matrix.hasOwnProperty("r_" + obj.id)){
                    this.matrix["r_" + obj.id] = {
                        top: top
                    };
                }
                this.widthMatrix = left;
                this.heightMatrix = top;
            }
            if (!this.matrix.hasOwnProperty("types_row"))
            {
                this.matrix["types_row"] = {};
            }
            this.matrix["types_row"][iteracion*2] = {
                type: 1,
                item_id: obj.id,
                resource: obj.id
            };
            this.matrix["types_row"][iteracion*2+1] = {
                type: 1,
                item_id: obj.id,
                resource: obj.id
            };
            let t = this.heightHeader+(2*iteracion+1+config.height_cell*iteracion)+(iteracion*config.height_cell);
            htmlLeft.push(<div key={"left_" + iteracion} style={{
                left: "1px",
                top: "" + t + "px",
                width: "274px",
                backgroundColor: "#dddddd",
                height: "" + (config.height_cell*2+1) + "px",
                position: "absolute",
                textAlign: "center"
            }} >{obj.name}</div>);
            TiktakComponent.pointRes[iteracion] = {
                type: 1,
                element: obj
            };
            iteracion++;
            for (let key in obj.children) {
                let child = obj.children[key];
                for (let i = 0; i < TiktakComponent.datesArray.length; i++){
                    let num = 0;
                    let left = i+config.width_cell*i;
                    let top = (2*iteracion+1+config.height_cell*iteracion)+(((iteracion+num)*config.height_cell)+num);
                    let style = TiktakComponent.cellStyle(left,top);
                    html.push(<div key={"l_" + i + "_t_" + iteracion + "_n_" + num} style={style}/>);
                    if (!this.matrix.hasOwnProperty(i)){
                        this.matrix[i] = {};
                    }
                    if (!this.matrix[i].hasOwnProperty("" + obj.id + "_" + child.id)){
                        this.matrix[i]["" + obj.id + "_" + child.id] = {};
                    }
                    this.matrix[i]["" + obj.id + "_" + child.id]["1"] = {
                        left: left,
                        top: top
                    };
                    num++;
                    left = i+config.width_cell*i;
                    top = (2*iteracion+1+config.height_cell*iteracion)+(((iteracion+num)*config.height_cell)+num);
                    style = TiktakComponent.cellStyle(left,top);
                    html.push(<div key={"l_" + i + "_t_" + iteracion + "_n_" + num} style={style}/>);
                    if (!this.matrix.hasOwnProperty("rn_" + obj.id + "_" + child.id)){
                        this.matrix["rn_" + obj.id + "_" + child.id] = {
                            'top': top
                        };
                    }
                    this.widthMatrix = left;
                    this.heightMatrix = top;
                }

                if (!this.matrix.hasOwnProperty("types_row"))
                {
                    this.matrix["types_row"] = {};
                }
                this.matrix["types_row"][iteracion*2] = {
                    type: 2,
                    item_id: child.id,
                    resource: obj.id
                };
                this.matrix["types_row"][iteracion*2+1] = {
                    type: 3,
                    item_id: child.id,
                    resource: obj.id
                };

                let t = this.heightHeader+(2*iteracion+1+config.height_cell*iteracion)+(((iteracion)*config.height_cell));
                htmlLeft.push(<div key={child.id + "_" + iteracion} style={{
                    left: "1px",
                    top: "" + t + "px",
                    width: "274px",
                    backgroundColor: "#ccc",
                    height: (config.height_cell*2+1) + "px",
                    position: "absolute"
                }} ><div style={{
                    height: (config.height_cell*2+1) + "px",
                    width: "50px",
                    float: "left",
                    backgroundColor: "#fff"
                }}/><div style={{
                    height: (config.height_cell*2+1) + "px",
                    width: "224px",
                    backgroundColor: "#eeeeee",
                    float: "right",
                    textAlign: "center"
                }}>{TiktakComponent.responsibles[child.id].name}</div></div>);
                TiktakComponent.pointRes[iteracion] = {
                    type: 2,
                    element: TiktakComponent.responsibles[child.id],
                    parent: obj
                };
                iteracion++;
            }
        }

        for (let key in TiktakComponent.resources) {
            let obj = TiktakComponent.resources[key];
            htmlResources.push(
                <TResource
                type={1}
                key={"tr_" + obj.id}
                matrix={this.matrix}>{obj}
                </TResource>);
        }
        this.widthMatrix += config.width_cell + 1;
        this.heightMatrix += config.height_cell + 1;
        console.log(this.matrix);
        //window.stop();
        //console.log(TiktakComponent.pointRes);
        return <div style={{height: this.props.height + "px", overflowY: "scroll"}}>
            <div style={{
                position: "relative",
                width: "276px",
                height: (this.heightHeader + this.heightMatrix) + "px",
                float: "left",
                backgroundColor: "#ccc"
            }}>{htmlLeft}</div>
            <div style={{
                overflowX: "scroll"
            }}>
                <div style={{
                    height: this.heightHeader + "px",
                    backgroundColor: "#cccccc",
                    position: 'relative',
                    textAlign: "center",
                    width: this.widthHeader + "px"
                }}>{htmlHead}</div>
                <div ref={"mainBlock"} style={{
                    position: "relative",
                    backgroundColor: "#cccccc",
                    height: this.heightMatrix + "px",
                    width: this.widthMatrix + "px"
                }}>
                    <div  style={{
                        position: "absolute",
                        height: this.heightMatrix + "px",
                        width: this.widthMatrix + "px",
                        backgroundColor: "#000",
                        visibility: "hidden"
                    }}/>
                    {/*<canvas id="example" style={{backgroundColor: "#fff"}}/>*/html}{htmlResources}
                    <TEvents key={"events"} setState={this.modify} events={TiktakComponent.events} matrix={this.matrix} style={{
                        position: "absolute",
                        height: this.heightMatrix + "px",
                        width: this.widthMatrix + "px",
                        zIndex: 1
                }}/>

                </div>
            </div>
        </div>
    }
}

export default TiktakComponent;